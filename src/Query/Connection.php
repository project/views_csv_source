<?php

namespace Drupal\views_csv_source\Query;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\views_csv_source\Event\PreCacheEvent;
use Drupal\views_csv_source\UriParserTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use League\Csv\Reader;
use League\Csv\Statement;
use Psr\Http\Message\ResponseInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Imitates the database connection, except for a CSV file.
 */
class Connection {

  // The URI parser trait.
  use UriParserTrait;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Connection constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param array $csvContent
   *   The content of the CSV file.
   * @param string $csvUri
   *   The URI of the CSV file.
   * @param array $csvHeader
   *   The headers of the CSV file.
   */
  public function __construct(
    protected CacheBackendInterface $cache,
    protected Client $httpClient,
    protected TimeInterface $time,
    protected EventDispatcherInterface $eventDispatcher,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    protected array $csvContent = [],
    protected string $csvUri = '',
    protected array $csvHeader = [],
  ) {
    $this->config = $config_factory->get('views_csv_source.settings');
    $this->logger = $logger_factory->get('views_csv_source');
  }

  /**
   * Select the CSV file.
   *
   * @param string $csv_uri
   *   The URI of the CSV file.
   * @param array $query_options
   *   The options for the query.
   *
   * @return \Drupal\views_csv_source\Query\Select
   *   The select object.
   */
  public function select(string $csv_uri, array $query_options): Select {
    $this->csvUri = $csv_uri;
    return new Select($this, $query_options);
  }

  /**
   * Parse the CSV URI.
   *
   * @param string $uri
   *   The URI of the CSV file.
   *
   * @return string
   *   The URI as a string.
   *
   * @throws \Exception
   *   If the file is not found.
   */
  public function parseCsvUri(string $uri = ''): string {
    if (empty($uri)) {
      $uri = $this->csvUri;
    }

    return static::getUriAsFilenameString($uri);
  }

  /**
   * Fetch the content of the CSV file.
   *
   * @param string $uri
   *   The URI of the CSV file.
   * @param array $options
   *   The options for the request.
   *
   * @return string
   *   The content of the CSV file.
   *
   * @throws \Exception
   *   If the file is not found.
   */
  public function fetchContent(string $uri, array $options = []): string {
    $uri = empty($options['csv_file']) ? $uri : $options['csv_file'];
    if (empty($uri)) {
      return '';
    }

    $unparsed_uri = $uri;
    if (!empty($this->csvContent[$unparsed_uri])) {
      return $this->csvContent[$unparsed_uri];
    }

    // This is probably a new connection let reset the csv content container.
    $this->csvContent = [];

    $scheme = parse_url($uri, PHP_URL_SCHEME);
    $uri = $this->parseCsvUri($uri);

    // Check for a local file.
    if ($scheme === 'internal' || $scheme === 'entity' || file_exists($uri)) {
      if (!file_exists($uri)) {
        throw new \Exception('Local file not found.');
      }
      $this->csvContent[$unparsed_uri] = file_get_contents($uri);
      return $this->csvContent[$unparsed_uri];
    }

    $cache_id = 'views_csv_source_' . md5($uri);
    if ($cache = $this->cache->get($cache_id)) {
      $this->csvContent[$unparsed_uri] = $cache->data;
      return $this->csvContent[$unparsed_uri];
    }

    $options += [
      'headers' => ['Content-Type' => 'application/csv'],
      'request_method' => 'get',
      'request_body' => '',
      'cache_id' => Crypt::randomBytesBase64(),
    ];

    try {
      // Add the request headers if available.
      $result = $this->getRequestResponse($uri, $options);
    }
    catch (GuzzleException $e) {
      $result = NULL;
      $this->logger->error($e->getMessage());
    }

    if (isset($result->error) || $result === NULL) {
      $message = sprintf('HTTP response: %s. URI: %s', $result->error ?? 'Unknown', $uri);
      throw new \Exception($message);
    }

    $cache_duration = (int) $this->config->get('cache_ttl');
    $csv_content = (string) $result->getBody();
    if ($cache_duration === 0) {
      $this->csvContent[$unparsed_uri] = $csv_content;
      return $this->csvContent[$unparsed_uri];
    }

    $cache_ttl = $this->time->getRequestTime() + $cache_duration;

    // Dispatch event before caching csv_content.
    $event = new PreCacheEvent($options['cache_id'], $csv_content);
    $this->eventDispatcher->dispatch($event, PreCacheEvent::VIEWS_CSV_SOURCE_PRE_CACHE);
    $this->csvContent[$unparsed_uri] = $event->getData();

    $this->cache->set($cache_id, $this->csvContent[$unparsed_uri], $cache_ttl);

    return $this->csvContent[$unparsed_uri];
  }

  /**
   * Get the headers from the CSV file.
   *
   * @param string $uri
   *   The URI of the CSV file.
   * @param array $query_options
   *   The options for the query.
   *
   * @return array
   *   The headers of the CSV file.
   */
  public function getCsvHeader(string $uri = '', array $query_options = []): array {
    $uri = empty($uri) ? $this->csvUri : $uri;
    if (empty($uri)) {
      return [];
    }

    if (!empty($this->csvHeader[$uri])) {
      return $this->csvHeader[$uri];
    }
    $this->csvHeader = [];
    $options = $query_options += [
      'delimiter' => ',',
      'header_index' => 0,
    ];

    try {
      $csv = Reader::createFromString($this->fetchContent($uri, $options));
      // Avoid warnings in PHP 8.4.
      $csv->setEscape('');
      $csv->setHeaderOffset($options['header_index']);
      $headers = $csv->getHeader() ?? [];
      $this->csvHeader[$uri] = $headers ? array_filter($headers) : [];
      return $this->csvHeader[$uri];
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * Get the column data from the CSV file with the given options.
   *
   * @param string $uri
   *   The URI of the CSV file.
   * @param string $header
   *   The header of the CSV file.
   * @param array $query_options
   *   The options for the query.
   *
   * @return array
   *   The column data from the CSV file.
   */
  public function getCsvColumnData(string $uri = '', string $header = '', array $query_options = []): array {
    $uri = empty($uri) ? $this->csvUri : $uri;
    if (empty($uri) || !$header) {
      return [];
    }

    $options = $query_options += [
      'delimiter' => ',',
      'header_index' => 0,
    ];

    try {
      $csv = Reader::createFromString($this->fetchContent($uri, $options));
      $csv->setHeaderOffset($options['header_index']);

      $all_headers = $csv->getHeader();
      // Ensuring that the selected header is part of CSV headers and removing
      // empty column headers.
      $selected_headers = array_intersect(array_filter($all_headers), [$header]);
      if (!$selected_headers) {
        return [];
      }

      $csv->mapHeader($selected_headers);
      $stmt = Statement::create();
      // Adding offset when header index is not on the first row.
      if ($options['header_index'] > 0) {
        $stmt = $stmt->offset($options['header_index']);
      }

      $records = $stmt->process($csv, $selected_headers);
      $column_contents = [];
      foreach ($records->fetchColumnByName($header) as $column_content) {
        $column_contents[] = $column_content;
      }
      return array_unique($column_contents);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return [];
    }
  }

  /**
   * Get request result.
   *
   * @param string $uri
   *   The request uri.
   * @param array $settings
   *   The request headers.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The request response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function getRequestResponse(string $uri, array $settings): ResponseInterface {
    if ($settings['request_method'] === 'post') {
      $options = [];
      $options['headers'] = $settings['headers'];
      if (!empty($options['request_body'])) {
        $options['multipart'] = Json::decode($settings['request_body']);
      }
      return $this->httpClient->post($uri, $options);
    }

    return $this->httpClient->get($uri, ['headers' => $settings['headers']]);
  }

}
