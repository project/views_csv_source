<?php

namespace Drupal\views_csv_source\Query;

use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use League\Csv\Reader;
use League\Csv\ResultSet;
use League\Csv\Statement;

/**
 * Defines a class for building and executing a select query.
 */
class Select {

  use DependencySerializationTrait;

  /**
   * The direction for ascending order.
   */
  const DIRECTION_ASC = 'ASC';

  /**
   * The direction for descending order.
   */
  const DIRECTION_DESC = 'DESC';

  /**
   * The JSON representation of the query.
   */
  protected string $json = '{}';

  /**
   * The query metadata for alter purposes.
   */
  public array $alterMetaData;

  /**
   * Indicates if preExecute() has already been called.
   */
  protected bool $prepared = FALSE;

  /**
   * The connection to use.
   */
  protected array|\Iterator $records;

  /**
   * The grouped and aggregated records.
   */
  protected array $groupedAndAggregatedRecords = [];

  /**
   * Flag for whether execute() was already called for this query.
   *
   * @var bool
   */
  protected bool $executed = FALSE;

  /**
   * Select constructor.
   *
   * @param \Drupal\views_csv_source\Query\Connection $connection
   *   The connection to use.
   * @param array $queryOptions
   *   The query options.
   */
  public function __construct(protected Connection $connection, protected array $queryOptions) {}

  /**
   * Check if the query is prepared.
   *
   * @return bool
   *   TRUE if the query is prepared, FALSE otherwise.
   */
  public function isPrepared(): bool {
    return $this->prepared;
  }

  /**
   * Prepare the query.
   *
   * @param \Drupal\views_csv_source\Query\Select|null $query
   *   The query to prepare.
   *
   * @return bool
   *   TRUE if the query is prepared, FALSE otherwise.
   */
  public function preExecute(?Select $query = NULL): bool {
    if (!isset($query)) {
      $query = $this;
    }

    // Only execute this once.
    if ($query->isPrepared()) {
      return TRUE;
    }

    $this->prepared = TRUE;
    return $this->prepared;
  }

  /**
   * Execute the query.
   *
   * @return array|\Iterator
   *   The records.
   *
   * @throws \League\Csv\Exception
   *   If the CSV reader cannot be built.
   */
  public function execute(): Select|\Iterator|array {
    if ($this->hasExecuted()) {
      return $this->isCountQuery() ? $this : $this->records;
    }

    $this->executed = TRUE;

    return $this->isCountQuery() ? $this : $this->getRecords();
  }

  /**
   * Fetch a field from the query.
   *
   * @return array|\Iterator|int
   *   The field.
   *
   * @throws \League\Csv\Exception
   *   If the CSV reader cannot be built.
   */
  public function fetchField(): array|\Iterator|int {
    $records = $this->getRecords();
    if ($this->isCountQuery()) {
      return is_array($records) ? count($records) : iterator_count($records);
    }

    return $records;
  }

  /**
   * Check if the query is a count query.
   *
   * @return bool
   *   TRUE if the query is a count query, FALSE otherwise.
   */
  public function isCountQuery(): bool {
    $is_count = FALSE;
    if ($count_item = $this->getObjectItem('count')) {
      $is_count = !empty($count_item[0]) && ($count_item[0] === 'true' || $count_item[0] == 1);
    }
    return $is_count;
  }

  /**
   * Add a field to the query.
   *
   * @param string $field
   *   The field to add.
   * @param string $function
   *   The function to apply to the field.
   * @param string $alias
   *   The field alias.
   *
   * @return $this
   *   The current object.
   */
  public function addField(string $field, string $function = '', string $alias = ''): static {
    $alias = $alias ?: $field;
    $column = new \stdClass();
    $column->name = $field;
    $column->function = $function;
    $column->alias = $alias;
    $this->addColumn($column);

    // Remove the added field from a condition and orderBy columns array if it
    // was added in "condition_order_by_columns" object item.
    $this->removeConditionOrderByColumn($column->name);

    // Ensure that a field with group_by function is also added to the group by
    // columns.
    if ($function === 'group_by') {
      $this->groupBy($field, $alias);
    }
    return $this;
  }

  /**
   * Add a group by to the query.
   *
   * @param string $field
   *   The field to group by.
   * @param string $alias
   *   The field alias.
   *
   * @return $this
   *   The current object.
   */
  public function groupBy(string $field, string $alias = ''): static {
    $group_by = $this->getObjectItem('group_by');
    $alias = $alias ?: $field;
    if (!isset($group_by[$alias])) {
      $group_by[$alias] = $field;
    }
    return $this->setObjectItem('group_by', $group_by);
  }

  /**
   * Add an order by to the query.
   *
   * @param string $column
   *   The column to order by.
   * @param string $direction
   *   The direction to order by.
   *
   * @return $this
   *   The current object.
   */
  public function orderBy(string $column, string $direction = self::DIRECTION_ASC): static {
    $data = $this->getObjectItem('order_by');
    $order_by = new \stdClass();
    $order_by->column = $column;
    $order_by->direction = $direction;
    $data[] = $order_by;

    // Ensure that the orderBy column is added to columns so that it can be
    // selected when choosing the columns to work with.
    $this->addConditionOrderByColumn($column);

    return $this->setObjectItem('order_by', $data);
  }

  /**
   * Set the range.
   *
   * @param int $offset
   *   The offset.
   * @param int $limit
   *   The limit.
   *
   * @return $this
   *   The current object.
   */
  public function range(int $offset, int $limit): static {
    $range = (object) $this->getObjectItem('range');
    $range->offset = $offset;
    $range->limit = $limit;
    return $this->setObjectItem('range', $range);
  }

  /**
   * Add a tag to the query.
   *
   * @param string $tag
   *   The tag to add.
   *
   * @return $this
   *   The current object.
   */
  public function addTag(string $tag): static {
    $tags = $this->getObjectItem('tags');
    if (!in_array($tag, $tags)) {
      $tags[] = $tag;
    }
    return $this->setObjectItem('tags', $tags);
  }

  /**
   * Set the limit.
   *
   * @param int $limit
   *   The limit.
   *
   * @return $this
   *   The current object.
   */
  public function setLimit(int $limit): static {
    $range = (object) $this->getObjectItem('range');
    $range->limit = $limit;
    return $this->setObjectItem('range', $range);
  }

  /**
   * Set the offset.
   *
   * @param int $offset
   *   The offset.
   *
   * @return $this
   *   The current object.
   */
  public function setOffset(int $offset): static {
    $range = (object) $this->getObjectItem('range');
    $range->offset = $offset;
    return $this->setObjectItem('range', $range);
  }

  /**
   * Get the limit.
   *
   * @return int
   *   The limit.
   */
  public function getLimit(): int {
    $range = (object) $this->getObjectItem('range');
    return $range->limit ?? -1;
  }

  /**
   * Get the offset.
   *
   * @return int
   *   The offset.
   */
  public function getOffset(): int {
    $range = (object) $this->getObjectItem('range');
    return $range->offset ?? 0;
  }

  /**
   * Add a condition to the query.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $condition
   *   The condition to add.
   *
   * @return $this
   *   The current object.
   */
  public function condition(ConditionInterface $condition): static {
    $where = [];
    $conditions = $condition->conditions();
    if (!empty($conditions['#conjunction'])) {
      $where['conjunction'] = $conditions['#conjunction'];
      unset($conditions['#conjunction']);
      $where['conditions'] = [];
      foreach ($conditions as $condition) {
        $where['conditions'][] = $this->buildConditions($condition);
      }
    }

    return $this->setObjectItem('where', $where);
  }

  /**
   * Add metadata to the query.
   *
   * @param string $key
   *   The key of the metadata to add.
   * @param mixed $object
   *   The metadata to add.
   *
   * @return $this
   *   The current object.
   */
  public function addMetaData(string $key, mixed $object): static {
    $this->alterMetaData[$key] = $object;
    return $this;
  }

  /**
   * Get the metadata.
   *
   * @param string $key
   *   The key of the metadata to get.
   *
   * @return mixed
   *   The metadata.
   */
  public function getMetaData(string $key): mixed {
    return $this->alterMetaData[$key] ?? NULL;
  }

  /**
   * Check if the query is empty.
   *
   * @return bool
   *   TRUE if the query is empty, FALSE otherwise.
   */
  public function isEmpty(): bool {
    return empty($this->json);
  }

  /**
   * Get the count query.
   *
   * @return $this
   *   The count query.
   */
  public function countQuery(): static {
    $query = clone $this;
    $query->setCount();
    return $query;
  }

  /**
   * Set the count flag.
   *
   * @return $this
   *   The current object.
   */
  private function setCount(): static {
    return $this->setObjectItem('count', TRUE);
  }

  /**
   * Determines whether this query has been executed already.
   *
   * @return bool
   *   TRUE when the query has been executed, FALSE otherwise.
   */
  public function hasExecuted(): bool {
    return $this->executed;
  }

  /**
   * Get the records.
   *
   * @return array|\Iterator
   *   The records.
   *
   * @throws \League\Csv\Exception
   *   If the CSV reader cannot be built.
   */
  private function getRecords(): array|\Iterator {
    $field_keys = array_unique(
      array_map(fn($field) => $field['name'], $this->getSelectedFields())
    );
    // Add the condition columns as part of fields so that filters can be
    // applied on them.
    $field_keys = array_merge(array_values($field_keys), $this->getConditionOrderByColumns());
    if (!$field_keys) {
      return [];
    }

    $uri = $this->connection->parseCsvUri();
    $options = $this->queryOptions += [
      'delimiter' => ',',
      'header_index' => 0,
    ];
    $csv_content = $this->connection->fetchContent($uri, $options);
    if (!$csv_content) {
      return [];
    }

    $csv = Reader::createFromString($csv_content);
    $csv->setHeaderOffset($options['header_index']);
    $csv->setDelimiter($options['delimiter']);

    // Process CSV Headers.
    // @todo maybe find a way to remove duplicates? Right now only removing
    // empty columns.
    if (!($all_headers = $csv->getHeader())) {
      return [];
    }
    // 1. Map headers to ensure that we only keep the column we need.
    $selected_column_headers = array_intersect(array_filter($all_headers), $field_keys);
    $result_set = $this->mapHeaders($csv, $selected_column_headers, $options['header_index']);

    // 2. Filter the result set.
    $result_set = $this->applyFilters($result_set);
    if ($result_set->count() === 0) {
      return [];
    }

    // 3. Sort the result set if this is not a count query.
    if (!$this->isCountQuery()) {
      $result_set = $this->applyOrderBy($result_set);
    }

    // 4. Group the result set and create a new one based on the grouping.
    if ($group_by = $this->getObjectItem('group_by')) {
      $result_set = $this->applyGroupBy($result_set, $group_by, $this->getSelectedAggregatedFields());
    }

    // 5. No need for offset and limit on a count query.
    if ($this->isCountQuery()) {
      $this->records = $result_set->getRecords();
      return $this->records;
    }

    // 6. Apply offset and limit.
    $stmt = Statement::create();
    $offset = $this->getOffset();
    if ($offset > 0) {
      $stmt = $stmt->offset($offset);
    }
    $limit = $this->getLimit();
    if ($limit > 0) {
      $stmt = $stmt->limit($limit);
    }

    $result_set = $stmt->process($result_set);
    $this->records = $result_set->getRecords();
    return $this->records;
  }

  /**
   * Get the JSON representation of the query.
   *
   * @return string
   *   The JSON representation of the query.
   */
  public function __toString(): string {
    return $this->json;
  }

  /**
   * Maps the headers to a CSV reader.
   *
   * @param \League\Csv\Reader $reader
   *   The CSV reader to map the headers to.
   * @param array $headers
   *   The headers to map to the CSV reader.
   * @param int $header_index
   *   The header index to user for offset.
   *
   * @return \League\Csv\ResultSet
   *   A new result set with the headers mapped to it.
   *
   * @throws \League\Csv\Exception
   * @throws \League\Csv\InvalidArgument
   * @throws \League\Csv\SyntaxError
   * @throws \ReflectionException
   */
  private function mapHeaders(Reader $reader, array $headers, int $header_index): ResultSet {
    // Using the header index to offset some of the rows that might not be
    // needed.
    return Statement::create()->offset($header_index)->process($reader, $headers);
  }

  /**
   * Filters the result set.
   *
   * @param \League\Csv\ResultSet $result_set
   *   The result set to filter.
   *
   * @return \League\Csv\ResultSet
   *   A filtered result set if where condition were added to the select query.
   *
   * @throws \League\Csv\Exception
   * @throws \League\Csv\InvalidArgument
   * @throws \League\Csv\SyntaxError
   * @throws \ReflectionException
   */
  protected function applyFilters(ResultSet $result_set): ResultSet {
    $stmt = Statement::create();
    if ($where = $this->getObjectItem('where')) {
      $stmt = $stmt->where(function (array $record) use ($where): bool {
        return $record && static::conditionPassed($record, (array) $where['conditions'], $where['conjunction']);
      });
    }

    return $stmt->process($result_set);
  }

  /**
   * Orders the result set.
   *
   * @param \League\Csv\ResultSet $result_set
   *   The result set to order.
   *
   * @return \League\Csv\ResultSet
   *   An ordered result set if "order by" was added to the query.
   *
   * @throws \League\Csv\Exception
   * @throws \League\Csv\InvalidArgument
   * @throws \League\Csv\SyntaxError
   * @throws \ReflectionException
   */
  protected function applyOrderBy(ResultSet $result_set): ResultSet {
    $stmt = Statement::create();
    if ($order_by = $this->getObjectItem('order_by')) {
      foreach ($order_by as $sort) {
        $stmt = $stmt->orderBy(function (array $a, array $b) use ($sort): int {
          $direction = strtoupper($sort->direction);
          return match ($direction) {
            self::DIRECTION_ASC => strnatcasecmp($a[$sort->column] ?? '', $b[$sort->column] ?? ''),
            self::DIRECTION_DESC => -strnatcasecmp($a[$sort->column] ?? '', $b[$sort->column] ?? ''),
            default => 0
          };
        });
      }
    }
    return $stmt->process($result_set);
  }

  /**
   * Aggregates and groups the result set.
   *
   * @param \League\Csv\ResultSet $result_set
   *   The set of results to group and aggregate.
   * @param array $group_by_columns
   *   The names of the columns to group by.
   * @param array $aggregated_columns
   *   The names of the columns to aggregate.
   *
   * @return \League\Csv\ResultSet
   *   A new result set that has been grouped and aggregated.
   *
   * @throws \League\Csv\Exception
   * @throws \League\Csv\InvalidArgument
   * @throws \League\Csv\SyntaxError
   * @throws \ReflectionException
   */
  protected function applyGroupBy(ResultSet $result_set, array $group_by_columns, array $aggregated_columns): ResultSet {
    // Ensure that all the columns that the grouping is going to apply to are
    // existing columns.
    if ($group_by_columns !== array_intersect($group_by_columns, $result_set->getHeader())) {
      return $result_set;
    }

    $result_set->each(function ($row) use ($group_by_columns, $aggregated_columns) {
      $grouped_by_values = static::extractGroupedColumnsValuesFromRecord($row, $group_by_columns);
      static::processGroupByAndAggregation($grouped_by_values, $group_by_columns, $aggregated_columns, $row);
      return TRUE;
    });

    // Trigger the "each" method predicate so that the
    // "groupedAndAggregatedRecords" property can be populated.
    Statement::create()->process($result_set);
    if (!$this->groupedAndAggregatedRecords) {
      return $result_set;
    }

    $records = [];
    $headers = [];
    foreach ($this->groupedAndAggregatedRecords as $record) {
      if (!$headers) {
        $headers = array_keys($record);
      }
      $records[] = array_values($record);
    }

    $tmp = new \SplTempFileObject();
    $tmp->fputcsv($headers);
    foreach ($records as $record) {
      $tmp->fputcsv($record);
    }
    $csv = Reader::createFromFileObject($tmp);
    $delimiter = $this->queryOptions['delimiter'] ?? ',';
    $csv->setDelimiter($delimiter);
    $csv->setHeaderOffset(0);
    return Statement::create()->process($csv, $headers);
  }

  /**
   * Add a column to the "fields" property array.
   *
   * @param \stdClass $column
   *   The column to add.
   *
   * @return $this
   *   The current object.
   */
  protected function addColumn(\stdClass $column): static {
    $fields = $this->getObjectItem('fields');
    $field_added = FALSE;
    foreach ($fields as &$existing_field) {
      if ($existing_field->alias === $column->alias && !$existing_field->function !== $column->function) {
        $existing_field->function = $column->function;
        $field_added = TRUE;
        break;
      }
    }

    if (!$field_added) {
      $fields[] = $column;
    }
    return $this->setObjectItem('fields', $fields);
  }

  /**
   * Adds a condition column to conditions columns.
   *
   * @param string $column
   *   The CSV column to add.
   *
   * @return $this
   *   The current object.
   */
  protected function addConditionOrderByColumn(string $column): static {
    // Only adding the column to condition fields if it's not part of selected
    // fields.
    foreach ($this->getSelectedFields() as $field) {
      if ($column === $field['name']) {
        return $this;
      }
    }

    $columns = $this->getConditionOrderByColumns();
    if (!in_array($column, $columns)) {
      $columns[] = $column;
    }
    return $this->setObjectItem('condition_order_by_columns', $columns);
  }

  /**
   * Removes a condition from column.
   *
   * @param string $column
   *   The CSV column to add.
   *
   * @return $this
   *   The current object.
   */
  protected function removeConditionOrderByColumn(string $column): static {
    $columns = $this->getConditionOrderByColumns();
    if ($column_index = array_search($column, $columns)) {
      unset($columns[$column_index]);
    }
    return $this->setObjectItem('condition_order_by_columns', array_values($columns));
  }

  /**
   * Gets the current stored condition columns.
   */
  protected function getConditionOrderByColumns(): array {
    return $this->getObjectItem('condition_order_by_columns');
  }

  /**
   * Get an item from the object.
   *
   * @param string $item_key
   *   The key of the item to get.
   *
   * @return array
   *   The item.
   */
  protected function getObjectItem(string $item_key): array {
    $object = (object) json_decode($this->json, FALSE);
    return (array) ($object->{$item_key} ?? []);
  }

  /**
   * Set an item in the object.
   *
   * @param string $item_key
   *   The key of the item to set.
   * @param mixed $item
   *   The item to set.
   *
   * @return $this
   *   The current object.
   */
  protected function setObjectItem(string $item_key, mixed $item): static {
    $object = (object) json_decode($this->json, FALSE);
    $object->{$item_key} = $item;
    $this->json = json_encode($object);
    return $this;
  }

  /**
   * Build the conditions' array.
   *
   * @param array $condition
   *   The condition to build.
   *
   * @return array
   *   The built condition.
   */
  protected function buildConditions(array $condition): array {
    $new_condition = [];
    $column = $condition['field'];
    if ($column instanceof ConditionInterface) {
      $conditions = $condition['field']->conditions();
      $new_condition['conjunction'] = $conditions['#conjunction'];
      unset($conditions['#conjunction']);
      $new_condition['conditions'] = [];
      foreach ($conditions as $cond) {
        $new_condition['conditions'][] = $this->buildConditions($cond);
      }
    }
    else {
      $this->addConditionOrderByColumn($column);
      $new_condition['column'] = $column;
      $new_condition['value'] = $condition['value'];
      $new_condition['operator'] = $condition['operator'];
    }
    return $new_condition;
  }

  /**
   * Check if the condition is met.
   *
   * @param array $record
   *   The record to check the condition against.
   * @param array $conditions
   *   The conditions to check.
   * @param string $conjunction
   *   The conjunction to use to check the conditions.
   * @param bool $checked
   *   The initial value of the check.
   *
   * @return bool
   *   TRUE if the condition is met, FALSE otherwise.
   */
  protected static function conditionPassed(array $record, array $conditions, string $conjunction, bool $checked = TRUE): bool {
    if ($conjunction === 'AND' && !$checked) {
      return FALSE;
    }

    foreach ($conditions as $condition) {
      $condition = (array) $condition;
      if (isset($condition['conjunction'])) {
        // Running a recursive call to check nested conditions.
        $checked = static::conditionPassed($record, $condition['conditions'], $condition['conjunction'], $checked);
      }
      elseif (!isset($record[$condition['column']])) {
        // The column in the condition is not in the record columns.
        $checked = TRUE;
      }
      else {
        $checked = static::verifyCondition($record[$condition['column']], $condition['value'], $condition['operator']);
      }

      // For AND conjunction, we shouldn't bail out as soon as we have a
      // negative check.
      if ($conjunction === 'AND' && !$checked) {
        return FALSE;
      }
      // For OR conjunction, we should also bail out as soon as we have a
      // positive check.
      if ($conjunction === 'OR' && $checked) {
        return TRUE;
      }
    }

    return $checked;
  }

  /**
   * Verify the condition.
   *
   * @param mixed $record_value
   *   The value of the record.
   * @param mixed $condition_value
   *   The value of the condition.
   * @param string $operator
   *   The operator to use to compare the values.
   *
   * @return bool
   *   TRUE if the condition is met, FALSE otherwise.
   */
  protected static function verifyCondition(mixed $record_value, mixed $condition_value, string $operator): bool {
    // @todo remember to introduce case insensitive checks.
    return match($operator) {
      '=' => $record_value == $condition_value,
      '!=', '<>' => $record_value != $condition_value,
      'contains' => stripos($record_value, $condition_value) !== FALSE,
      'starts' => str_starts_with($record_value, $condition_value),
      'not_starts', 'not starts' => !str_starts_with($record_value, $condition_value),
      'ends' => !(strlen($condition_value) > 0) || str_ends_with($record_value, $condition_value),
      'not_ends', 'not ends' => !str_ends_with($record_value, $condition_value),
      'not' => stripos($record_value, $condition_value) === FALSE,
      'shorterthan' => strlen($record_value) < $condition_value,
      'longerthan' => strlen($record_value) > $condition_value,
      'regular_expression' => preg_match($condition_value, $record_value) === 1,
      'in' => in_array($record_value, $condition_value),
      'not in' => !in_array($record_value, $condition_value),
      'contains any' => static::containsWordsConditionChecker($record_value, $condition_value),
      'contains none' => !static::containsWordsConditionChecker($record_value, $condition_value),
      'contains all' => static::containsWordsConditionChecker($record_value, $condition_value, TRUE),
      'empty', 'IS NULL' => empty($record_value),
      'not empty', 'IS NOT NULL' => !empty($record_value),
      // They gave us an operator we don't support.
      default => TRUE,
    };
  }

  /**
   * Extract the values of the grouped columns from the record.
   *
   * @param array $record
   *   The record to extract the values from.
   * @param array $grouped_columns
   *   The names of the grouped columns.
   *
   * @return array
   *   The values of the grouped columns.
   */
  protected static function extractGroupedColumnsValuesFromRecord(array $record, array $grouped_columns): array {
    return array_map(function ($column) use ($record) {
      return $record[$column];
    }, $grouped_columns);
  }

  /**
   * Get the selected fields.
   *
   * @return array
   *   The selected fields.
   */
  protected function getSelectedFields(): array {
    $fields = [];
    foreach ($this->getObjectItem('fields') as $field) {
      if (!empty($field->name)) {
        $fields[$field->alias] = (array) $field;
      }
    }
    return $fields;
  }

  /**
   * Process the group by and aggregation.
   *
   * @param array $group_by_values
   *   The values of the group by columns.
   * @param array $group_by_columns
   *   The names of the group by columns.
   * @param array $aggregated_columns
   *   The names of the aggregated columns.
   * @param array $row
   *   The row to process.
   */
  protected function processGroupByAndAggregation(array $group_by_values, array $group_by_columns, array $aggregated_columns, array $row): void {
    $key = implode('_____', $group_by_values);
    // Adding the grouping fields as part of the array before any aggregation.
    if (!isset($this->groupedAndAggregatedRecords[$key])) {
      $group_by_column_aliases = array_keys($group_by_columns);
      $this->groupedAndAggregatedRecords[$key] = array_combine($group_by_column_aliases, $group_by_values);
    }

    // Calculate aggregation if any.
    foreach ($aggregated_columns as $column_alias => $field) {
      $function = $field['function'];
      $column = $field['name'];
      if ($function === 'count') {
        $counter = $this->groupedAndAggregatedRecords[$key][$column_alias] ?? 0;
        $this->groupedAndAggregatedRecords[$key][$column_alias] = $counter + (isset($row[$column]) ? 1 : 0);
      }
      elseif ($function === 'sum' && is_numeric($row[$column])) {
        $sum = $this->groupedAndAggregatedRecords[$key][$column_alias] ?? 0;
        $this->groupedAndAggregatedRecords[$key][$column_alias] = $sum + $row[$column];
      }
      elseif ($function === 'min' && is_numeric($row[$column])) {
        $min = $this->groupedAndAggregatedRecords[$key][$column_alias] ?? $row[$column];
        $this->groupedAndAggregatedRecords[$key][$column_alias] = min($min, $row[$column]);
      }
      elseif ($function === 'max' && is_numeric($row[$column])) {
        $max = $this->groupedAndAggregatedRecords[$key][$column_alias] ?? $row[$column];
        $this->groupedAndAggregatedRecords[$key][$column_alias] = max($max, $row[$column]);
      }
      elseif ($function === 'avg' && is_numeric($row[$column])) {
        $index_sum = $column_alias . '_sum';
        $index_count = $column_alias . '_count';
        $sum = $this->groupedAndAggregatedRecords[$key][$index_sum] ?? 0;
        $count = $this->groupedAndAggregatedRecords[$key][$index_count] ?? 0;
        $sum += $row[$column];
        $count += 1;
        $this->groupedAndAggregatedRecords[$key][$index_sum] = $sum;
        $this->groupedAndAggregatedRecords[$key][$index_count] = $count;
        $this->groupedAndAggregatedRecords[$key][$column_alias] = $sum / $count;
      }
    }
  }

  /**
   * Combines "in" and "contains" operator to check both on an array condition.
   *
   * @param string $haystack
   *   The record value.
   * @param array $words
   *   The condition value.
   * @param bool $all
   *   The "all" flag to signal that all the words should be checked.
   *
   * @return bool
   *   The checked condition status.
   */
  protected static function containsWordsConditionChecker(string $haystack, array $words, bool $all = FALSE): bool {
    $checked = FALSE;
    foreach ($words as $word) {
      $checked = str_contains($haystack, $word);
      if ($checked && !$all) {
        return TRUE;
      }
      if ($all && !$checked) {
        return FALSE;
      }
    }
    return $checked;
  }

  /**
   * Gets the selected aggregated fields.
   *
   * @return array
   *   The aggregated fields.
   */
  protected function getSelectedAggregatedFields(): array {
    $selected_aggregation_fields = [];
    foreach ($this->getSelectedFields() as $field) {
      if (!empty($field['function']) && $field['function'] !== 'group_by') {
        $selected_aggregation_fields[$field['alias']] = $field;
      }
    }
    return $selected_aggregation_fields;
  }

}
