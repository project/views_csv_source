<?php

namespace Drupal\views_csv_source;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\file\FileInterface;

/**
 * Provides methods to parse URIs.
 */
trait UriParserTrait {

  /**
   * Gets the Uri as filename without the scheme.
   *
   * @param string $uri
   *   The uri.
   *
   * @return string
   *   The filename.
   *
   * @see static::getUserEnteredStringAsUri()
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function getUriAsFilenameString(string $uri): string {
    $scheme = parse_url($uri, PHP_URL_SCHEME);
    if ($scheme === 'internal') {
      return DRUPAL_ROOT . explode(':', $uri, 2)[1];
    }
    if ($scheme === 'entity' && ($entity = static::getFileEntityFromUri($uri))) {
      $uri = $entity->getFileUri();
      /** @var \Drupal\Core\File\FileSystemInterface $file_system */
      $file_system = \Drupal::service('file_system');
      return $file_system->realpath($uri);
    }
    return $uri;
  }

  /**
   * Gets the file entity from URI.
   *
   * @param string $uri
   *   The file uri.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file entity if found, NULL otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function getFileEntityFromUri(string $uri): ?FileInterface {
    [$entity_type, $entity_id] = explode('/', substr($uri, 7), 2);
    return $entity_type === 'file' ? \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id) : NULL;
  }

  /**
   * Gets the user-entered string as a URI.
   *
   * The following two forms of input are mapped to URIs:
   * - entity autocomplete ("label (entity id)") strings: to 'entity:' URIs;
   * - strings without a detectable scheme: to 'internal:' URIs.
   *
   * @param string $string
   *   The user-entered string.
   *
   * @return string
   *   The URI, if a non-empty $uri was passed.
   *
   * @see static::getUriAsDisplayableString()
   */
  protected static function getUserEnteredStringAsUri(string $string): string {
    $uri = trim($string);

    // Detect entity autocomplete string, map to 'entity:' URI.
    $file_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($string);
    if ($file_id !== NULL) {
      $uri = 'entity:file/' . $file_id;
    }
    // Detect a scheme-less string, map to 'internal:' URI.
    elseif (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      $uri = 'internal:' . $string;
    }
    return $uri;
  }

  /**
   * Gets the URI without the 'internal:' or 'entity:' scheme.
   *
   * The following two forms of URIs are transformed:
   * - 'entity:' URIs: to entity autocomplete ("label (entity id)") strings;
   * - 'internal:' URIs: the scheme is stripped.
   *
   * This method is the inverse of static::getUserEnteredStringAsUri().
   *
   * @param string $uri
   *   The URI to get the displayable string for.
   *
   * @return string
   *   The displayable string.
   *
   * @see static::getUserEnteredStringAsUri()
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected static function getUriAsDisplayableString(string $uri): string {
    $scheme = parse_url($uri, PHP_URL_SCHEME);
    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    if ($scheme === 'internal') {
      $displayable_string = explode(':', $uri, 2)[1];
    }
    elseif ($scheme === 'entity') {
      $entity = static::getFileEntityFromUri($uri);
      if ($entity instanceof FileInterface) {
        $displayable_string = EntityAutocomplete::getEntityLabels([$entity]);
      }
    }
    return $displayable_string;
  }

}
