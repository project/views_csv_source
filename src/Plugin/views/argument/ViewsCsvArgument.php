<?php

namespace Drupal\views_csv_source\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\StringArgument;
use Drupal\views_csv_source\Plugin\views\ColumnSelectorTrait;

/**
 * Base argument handler for views_csv_source.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("views_csv_source_argument")
 */
class ViewsCsvArgument extends StringArgument {

  use ColumnSelectorTrait;

  /**
   * Build the summary query based on a string.
   */
  protected function summaryQuery() {
    $this->ensureMyTable();
  }

  /**
   * Add this filter to the query.
   */
  public function query($group_by = FALSE) {
    $argument = $this->argument;
    if (!empty($this->options['transform_dash'])) {
      $argument = strtr($argument, '-', ' ');
    }

    if (!empty($this->options['break_phrase'])) {
      $this->unpackArgumentValue();
    }
    else {
      $this->value = [$argument];
      $this->operator = 'or';
    }

    $this->ensureMyTable();
    $field = '';
    if (empty($this->options['glossary'])) {
      $field = $this->options['key'];
    }

    if (count($this->value) > 1) {
      $argument = $this->value;
    }

    $this->query->addWhere(0, $field, $argument, '=');
  }

  /**
   * Generate the filter criteria.
   */
  public function generate() {
    $operator = "=";
    $key = $this->options['key'];
    $value = $this->argument;

    return [$key, $operator, $value];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['key'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form = $this->buildKeyOptionElement($form);
  }

}
