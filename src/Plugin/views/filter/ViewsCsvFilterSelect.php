<?php

namespace Drupal\views_csv_source\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Drupal\views_csv_source\Plugin\views\ColumnSelectorTrait;
use Drupal\views_csv_source\Plugin\views\query\ViewsCsvQuery;

/**
 * Filter handler for views_csv_source with options.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("views_csv_source_filter_select")
 */
class ViewsCsvFilterSelect extends InOperator {

  use ColumnSelectorTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['key']['default'] = '';
    $options['empty_cell_behavior']['default'] = 'none';
    $options['multi_value_cell_separator']['default'] = '';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function operators(): array {
    $operators = parent::operators();
    $operators['contains all'] = [
      'title' => $this->t('Contains all of these words'),
      'short' => $this->t('contains all'),
      'short_single' => $this->t('='),
      'method' => 'opSimple',
      'values' => 1,
    ];
    $operators['contains any'] = [
      'title' => $this->t('Contains any of these words'),
      'short' => $this->t('contains any'),
      'short_single' => $this->t('='),
      'method' => 'opSimple',
      'values' => 1,
    ];
    $operators['contains none'] = [
      'title' => $this->t('Contains none of these words'),
      'short' => $this->t('contains none'),
      'short_single' => $this->t('<>'),
      'method' => 'opSimple',
      'values' => 1,
    ];
    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $form = $this->buildKeyOptionElement($form);

    // Adding ajax behavior on the column key.
    $form['views_csv_source_filter_select_settings'] = [
      '#prefix' => '<div class="views-csv-source-select-filter-settings clearfix">',
      '#suffix' => '</div>',
      // Should always come after the expose button.
      '#weight' => -199,
      '#attached' => ['library' => ['views_csv_source/views-admin']],
      '#type' => 'container',
      '#attributes' => [
        'class' => ['js-only'],
        'data-views-csv-source-select-filter-settings' => TRUE,
      ],
    ];

    $form['views_csv_source_filter_select_settings']['key'] = $form['key'];
    unset($form['key']);

    $form['views_csv_source_filter_select_settings']['empty_cell_behavior'] = [
      '#type' => 'radios',
      '#title' => $this->t('Empty cell behavior'),
      '#options' => [
        'none' => $this->t('None'),
        'remove' => $this->t('Remove'),
        'add_label' => $this->t('Add empty label'),
      ],
      '#default_value' => $this->options['empty_cell_behavior'] ?? 'none',
      'none' => [
        '#description' => $this->t('When there is an empty cell, the select options list will display an option with an empty label as in the cell.'),
      ],
      'remove' => [
        '#description' => $this->t('This will remove the empty value from showing under the provided options. Can be useful if exposing the filter.'),
      ],
      'add_label' => [
        '#description' => $this->t('This option will keep the empty cell option as an option but provide an "- EMPTY -" label.'),
      ],
    ];

    $form['views_csv_source_filter_select_settings']['multi_value_cell_separator'] = [
      '#type' => 'select',
      '#title' => $this->t('Multi value cell separator'),
      '#options' => [
        '' => $this->t('None'),
        ' ' => $this->t('Space'),
        ',' => ',',
        '|' => '|',
        ';' => ';',
        '/' => '/',
        '.' => '.',
      ],
      '#default_value' => $this->options['multi_value_cell_separator'] ?? '',
      '#description' => $this->t('If selected, this value is used as a separator when exploding cell values to build options.'),
    ];

    // Refresh the value input based on the selected column using ajax.
    $form['views_csv_source_filter_select_settings']['button'] = [
      '#limit_validation_errors' => [],
      '#type' => 'submit',
      '#value' => $this->t('Refresh value options'),
      '#submit' => [[$this, 'refreshValueInput']],
    ];
  }

  /**
   * Submit handler to refresh the value options when the column key change.
   */
  public function refreshValueInput($form, FormStateInterface $form_state): void {
    $item = &$this->options;
    $view = $form_state->get('view');
    $display_id = $form_state->get('display_id');
    $type = $form_state->get('type');
    $id = $form_state->get('id');

    // If the user made a choice in the key field, then we need to build a list
    // of options from the selected column in the CSV file.
    $inputs = $form_state->getUserInput();
    foreach ($inputs['options']['views_csv_source_filter_select_settings'] as $key => $selected_setting) {
      if ($selected_setting !== $item[$key]) {
        $item[$key] = $selected_setting;
        // Reset eventual selected options values.
        $item['value'] = [];
      }
    }

    $view->getExecutable()->setHandler($display_id, $type, $id, $item);
    $view->addFormToStack($form_state->get('form_key'), $display_id, $type, $id, TRUE, TRUE);

    $form_state->set('rerender', TRUE);
    $form_state->setRebuild();
    // Write to cache.
    $view->cacheSet();
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = $this->t('Value');
    $this->definition['options callback'] = [$this, 'getColumnOptions'];

    $query = $this->view->getQuery();
    // Initialize the key to the first column header if empty.
    if ($this->options['key'] === '' && $headers = $query->getCsvHeader()) {
      $this->options['key'] = reset($headers);
    }
    // Pass the selected key to the options' getter argument.
    $this->definition['options arguments'] = [$this->options['key']];
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($this->options['key']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple(string $column_name = ''): void {
    if (empty($this->value) || !$column_name) {
      return;
    }

    // Ensuring that only values that are in the options are submitted.
    $submitted_values = array_values($this->value);
    $options = $this->getValueOptions();
    $values = array_intersect($submitted_values, $options);
    if (empty($values)) {
      return;
    }
    $this->query->addWhere($this->options['group'], $column_name, $values, $this->operator);
  }

  /**
   * {@inheritdoc}
   */
  protected function opEmpty(string $column_name = '') {
    if (!$column_name) {
      return;
    }

    $operator = $this->operator == 'empty' ? "IS NULL" : "IS NOT NULL";
    $this->query->addWhere($this->options['group'], $column_name, NULL, $operator);
  }

  /**
   * Get the column options.
   *
   * @param string $column
   *   The column name.
   *
   * @return array
   *   The column options.
   */
  protected function getColumnOptions(string $column): array {
    if (!$column) {
      return [];
    }

    $query = $this->view->getQuery();
    if (!$query instanceof ViewsCsvQuery) {
      return [];
    }

    $column_data = $query->getCsvColumnValues($column);
    $cell_value_separator = $this->options['multi_value_cell_separator'] ?? '';
    if ($column_data && $cell_value_separator) {
      $separated_data = [];
      $is_space_separator = $cell_value_separator === ' ';
      foreach ($column_data as $column_value) {
        $datum = !$is_space_separator ?
          array_map('trim', explode($cell_value_separator, $column_value)) :
          explode($cell_value_separator, $column_value);
        $separated_data = array_merge($separated_data, $datum);
      }
      $column_data = array_unique($separated_data);
    }

    $options = array_combine($column_data, $column_data);
    ksort($options);

    // Apply empty cell behavior if needed.
    $empty_cell_behavior = $this->options['empty_cell_behavior'];
    if ($empty_cell_behavior !== 'none') {
      foreach ($options as $key => $label) {
        if ($key === '' || $key === NULL) {
          if ($empty_cell_behavior === 'remove') {
            unset($options[$key]);
          }
          else {
            $options[$key] = $this->t('- EMPTY -');
          }
          break;
        }
      }
    }

    return $options;
  }

}
