<?php

namespace Drupal\views_csv_source\Plugin\views\filter;

/**
 * Trait to add options to the filter handler.
 */
trait ViewsCsvFilterTrait {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['key']['default'] = '';
    return $options;
  }

}
