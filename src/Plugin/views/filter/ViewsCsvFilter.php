<?php

namespace Drupal\views_csv_source\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views_csv_source\Plugin\views\ColumnSelectorTrait;

/**
 * Base filter handler for views_csv_source.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("views_csv_source_filter")
 */
class ViewsCsvFilter extends FilterPluginBase {

  use ColumnSelectorTrait;
  use ViewsCsvFilterTrait;

  /**
   * Exposed filter options.
   *
   * @var bool
   */
  protected $alwaysMultiple = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['expose']['contains']['required'] = ['default' => FALSE];
    $options['expose']['contains']['placeholder'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultExposeOptions() {
    parent::defaultExposeOptions();
    $this->options['expose']['placeholder'] = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);
    $form['expose']['placeholder'] = [
      '#type' => 'textfield',
      '#default_value' => $this->options['expose']['placeholder'],
      '#title' => $this->t('Placeholder'),
      '#size' => 40,
      '#description' => $this->t('Hint text that appears inside the field when empty.'),
    ];
  }

  /**
   * Operators.
   *
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   *
   * @return array
   *   The operators.
   */
  public function operators(): array {
    $operators = [
      '=' => [
        'title' => $this->t('Is equal to'),
        'short' => $this->t('='),
        'method' => 'opEqual',
        'values' => 1,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'short' => $this->t('!='),
        'method' => 'opEqual',
        'values' => 1,
      ],
      'contains' => [
        'title' => $this->t('Contains'),
        'short' => $this->t('contains'),
        'method' => 'opContains',
        'values' => 1,
      ],
      'starts' => [
        'title' => $this->t('Starts with'),
        'short' => $this->t('begins'),
        'method' => 'opStartsWith',
        'values' => 1,
      ],
      'not_starts' => [
        'title' => $this->t('Does not start with'),
        'short' => $this->t('not_begins'),
        'method' => 'opNotStartsWith',
        'values' => 1,
      ],
      'ends' => [
        'title' => $this->t('Ends with'),
        'short' => $this->t('ends'),
        'method' => 'opEndsWith',
        'values' => 1,
      ],
      'not_ends' => [
        'title' => $this->t('Does not end with'),
        'short' => $this->t('not_ends'),
        'method' => 'opNotEndsWith',
        'values' => 1,
      ],
      'not' => [
        'title' => $this->t('Does not contain'),
        'short' => $this->t('!has'),
        'method' => 'opNotLike',
        'values' => 1,
      ],
      'shorterthan' => [
        'title' => $this->t('Length is shorter than'),
        'short' => $this->t('shorter than'),
        'method' => 'opShorterThan',
        'values' => 1,
      ],
      'longerthan' => [
        'title' => $this->t('Length is longer than'),
        'short' => $this->t('longer than'),
        'method' => 'opLongerThan',
        'values' => 1,
      ],
      'regular_expression' => [
        'title' => $this->t('Regular expression'),
        'short' => $this->t('regex'),
        'method' => 'opRegex',
        'values' => 1,
      ],
    ];
    // If the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += [
        'empty' => [
          'title' => $this->t('Is empty (NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('empty'),
          'values' => 0,
        ],
        'not empty' => [
          'title' => $this->t('Is not empty (NOT NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('not empty'),
          'values' => 0,
        ],
      ];
    }

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function operatorOptions($which = 'title'): array {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state): void {
    // We have to make some choices when creating this as an exposed
    // filter form. For example, if the operator is locked and thus
    // not rendered, we can't render dependencies; instead we only
    // render the form items we need.
    $which = 'all';
    $source = '';
    if (!empty($form['operator'])) {
      $source = ':input[name="options[operator]"]';
    }
    if ($exposed = $form_state->get('exposed')) {
      $identifier = $this->options['expose']['identifier'];

      if (empty($this->options['expose']['use_operator']) || empty($this->options['expose']['operator_id'])) {
        // Exposed and locked.
        $which = in_array($this->operator, $this->operatorValues(1)) ? 'value' : 'none';
      }
      else {
        $source = ':input[name="' . $this->options['expose']['operator_id'] . '"]';
      }
    }

    if ($which == 'all' || $which == 'value') {
      $form['value'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Value'),
        '#size' => 30,
        '#default_value' => $this->value,
      ];
      if (!empty($this->options['expose']['placeholder'])) {
        $form['value']['#attributes']['placeholder'] = $this->options['expose']['placeholder'];
      }
      $user_input = $form_state->getUserInput();
      if ($exposed && !isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value;
        $form_state->setUserInput($user_input);
      }

      if ($which == 'all') {
        // Setup #states for all operators with one value.
        foreach ($this->operatorValues(1) as $operator) {
          $form['value']['#states']['visible'][] = [
            $source => ['value' => $operator],
          ];
        }
      }
    }

    if (!isset($form['value'])) {
      // Ensure there is something in the 'value'.
      $form['value'] = [
        '#type' => 'value',
        '#value' => NULL,
      ];
    }
  }

  /**
   * Get the operator values.
   *
   * @param int $values
   *   The number of values.
   *
   * @return array
   *   The operator values.
   */
  protected function operatorValues(int $values = 1): array {
    $options = [];
    foreach ($this->operators() as $id => $info) {
      if (isset($info['values']) && $info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $form = $this->buildKeyOptionElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $this->ensureMyTable();
    $selected_column = $this->options['key'];
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      if (is_array($this->value)) {
        $this->value = $this->value[0];
      }
      $this->{$info[$this->operator]['method']}($selected_column);
    }
  }

  /**
   * Generate the filter criteria.
   *
   * @return array
   *   The filter criteria.
   */
  public function generate(): array {
    $options = $this->options;

    $operator = $this->options['operator'];
    if ($options['exposed'] && $options['expose']['use_operator']) {
      $operator = $this->operator;
    }

    $value = $this->options['value'];
    if ($options['exposed'] && !empty($this->value)) {
      $value = $options['expose']['multiple'] ? $this->value : reset($this->value);
    }

    return !empty($value) ? [$this->options['key'], $operator, $value] : [];
  }

  /**
   * Adds a where clause for the operation, 'equals'.
   *
   * @param string $column_name
   *   The column name.
   */
  public function opEqual(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, $this->operator);
  }

  /**
   * Adds a where clause for the operation, 'contains'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opContains(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'contains');
  }

  /**
   * Adds a where clause for the operation, 'starts'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opStartsWith(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'starts');
  }

  /**
   * Adds a where clause for the operation, 'not_starts'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opNotStartsWith(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'not_starts');
  }

  /**
   * Adds a where clause for the operation, 'ends'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opEndsWith(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'ends');
  }

  /**
   * Adds a where clause for the operation, 'not_ends'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opNotEndsWith(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'not_ends');
  }

  /**
   * Adds a where clause for the operation, 'empty'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opNotLike(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'not');
  }

  /**
   * Adds a where clause for the operation, 'empty'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opShorterThan(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'shorterthan');
  }

  /**
   * Adds a where clause for the operation, 'empty'.
   *
   * @param string $column_name
   *   The column name.
   */
  protected function opLongerThan(string $column_name): void {
    $this->query->addWhere($this->options['group'], $column_name, $this->value, 'longerthan');
  }

  /**
   * Adds a where clause for the operation, 'empty'.
   *
   * @param string $field
   *   The column name.
   */
  protected function opRegex(string $field): void {
    $this->query->addWhere($this->options['group'], $field, $this->value, 'regular_expression');
  }

}
