<?php

namespace Drupal\views_csv_source\Plugin\views\query;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views_csv_source\Query\Connection;
use Drupal\views_csv_source\Query\Select;
use Drupal\views_csv_source\UriParserTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base query handler for views_csv_source.
 *
 * @ViewsQuery(
 *   id = "views_csv_source_query",
 *   title = @Translation("Views CSV Source Query"),
 *   help = @Translation("Query against API(CSV).")
 * )
 */
class ViewsCsvQuery extends QueryPluginBase {

  use UriParserTrait;

  /**
   * To store the contextual Filter info.
   *
   * @var array
   */
  protected $contextualFilter;

  /**
   * To store the url filter info.
   *
   * @var array
   */
  protected $urlParams;

  /**
   * An array of fields.
   *
   * @var array
   */
  public array $fields = [];

  /**
   * A simple array of order by clauses.
   *
   * @var array
   */
  public array $orderby = [];

  /**
   * A simple array of group by clauses.
   *
   * @var array
   */
  public array $groupby = [];

  /**
   * Not actually used.
   *
   * Copied over from \Drupal\views\Plugin\views\query\Sql::$where since
   * QueryPluginBase depends on it.
   *
   * @var array
   */
  public array $where = [];

  /**
   * A simple array of filter clauses.
   *
   * @var array
   */
  public array $filter = [];

  /**
   * If the query has an aggregate function.
   *
   * @var bool
   */
  protected bool $hasAggregate = FALSE;

  /**
   * Query tags which will be passed over to the query object.
   *
   * @var array
   */
  public array $tags = [];

  /**
   * Should this query be optimized for counts, for example no sorts.
   *
   * @var bool
   */
  protected $getCountOptimized = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MessengerInterface $messenger,
    protected Token $token,
    protected LoggerInterface $logger,
    protected Connection $connection,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('messenger'),
      $container->get('token'),
      $container->get('logger.channel.views_csv_source'),
      $container->get('views_csv_source.connection'),
    );
  }

  /**
   * Plays a similar role to ensure_table() in Sql.
   *
   * @param string $table
   *   The table to ensure.
   * @param mixed $relationship
   *   The relationship to ensure.
   *
   * @return string
   *   The table type.
   */
  public function ensureTable(string $table, mixed $relationship = NULL): string {
    return 'csv';
  }

  /**
   * Generate a query from all the information supplied to the object.
   *
   * @param bool $get_count
   *   Provide a count query if this is true, otherwise provide a normal query.
   */
  public function query($get_count = FALSE) {
    // An optimized count query includes just the base field instead of all the
    // fields. Determine of this query qualifies by checking for a groupby.
    if ($get_count && !$this->groupby) {
      foreach ($this->fields as $field) {
        if (!empty($field['function'])) {
          $this->getCountOptimized = FALSE;
          break;
        }
      }
    }
    else {
      $this->getCountOptimized = FALSE;
    }
    if (!isset($this->getCountOptimized)) {
      $this->getCountOptimized = TRUE;
    }

    $query = $this->connection
      ->select($this->getCsvFileUri(), $this->getQueryOptions())
      ->addTag('views')
      ->addTag('views_' . $this->view->storage->id());

    // Add the tags added to the view itself.
    foreach ($this->tags as $tag) {
      $query->addTag($tag);
    }

    // Assemble the group by clause, if any.
    $this->hasAggregate = FALSE;
    $non_aggregates = $this->getNonAggregates();
    if (!$this->hasAggregate) {
      // Allow 'GROUP BY' even no aggregation function has been set.
      $this->hasAggregate = $this->view->display_handler->getOption('group_by');
    }
    $group_by = [];
    if ($this->hasAggregate && (!empty($this->groupby) || !empty($non_aggregates))) {
      $group_by = array_unique(array_merge($this->groupby, $non_aggregates));
    }

    // Add all fields to the query.
    $this->compileFields($query);

    // Add a group by.
    if ($group_by) {
      foreach ($group_by as $alias => $field) {
        $query->groupBy($field, $alias);
      }
    }

    if (!$this->getCountOptimized) {
      // We only add the order by if we're not counting.
      if ($this->orderby) {
        foreach ($this->orderby as $order) {
          $query->orderBy($order['field'], $order['order']);
        }
      }
    }

    if (!empty($this->where) && $condition = $this->buildCondition('where')) {
      $query->condition($condition);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ViewExecutable $view) {
    \Drupal::moduleHandler()->invokeAll('views_query_alter', [$view, $this]);
  }

  /**
   * Builds the necessary info to execute the query.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view object.
   */
  public function build(ViewExecutable $view): void {
    // Store the view in the object to be able to use it later.
    $this->view = $view;

    $view->initPager();

    // Let the pager modify the query to add limits.
    $view->pager->query();

    $view->build_info['query'] = $this->query();
    $view->build_info['count_query'] = $this->query(TRUE);

    $view->build_info['query_args'] = [];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view): void {
    $query = $view->build_info['query'];
    $count_query = $view->build_info['count_query'];

    $query->addMetaData('view', $view);
    $count_query->addMetaData('view', $view);

    if ($query->isEmpty()) {
      $start = microtime(TRUE);
      $view->execute_time = microtime(TRUE) - $start;
      return;
    }

    $count_query->preExecute();
    // Build the count query.
    $count_query = $count_query->countQuery();

    $start = microtime(TRUE);

    try {
      if ($view->pager->useCountQuery() || !empty($view->get_total_rows)) {
        $view->pager->executeCountQuery($count_query);
      }

      // Let the pager modify the query to add limits.
      $view->pager->preExecute($query);

      if (!empty($this->limit) || !empty($this->offset)) {
        // We can't have an offset without a limit, so provide a very large
        // limit instead.
        $limit = intval(!empty($this->limit) ? $this->limit : 999999);
        $offset = intval(!empty($this->offset) ? $this->offset : 0);
        $query->range($offset, $limit);
      }

      $records = $query->execute();
      $result = [];
      $index = 0;
      $column_aliases = $this->getColumnAliasesAsKeyValues();
      foreach ($records as $row) {
        $new_row = $this->buildRowFromResultRow($row, $column_aliases);
        $new_row->index = $index++;
        $result[] = $new_row;
      }

      $view->result = $result;
      array_walk($view->result, function (ResultRow $row, $index) {
        $row->index = $index;
      });

      $view->pager->postExecute($view->result);
      $view->pager->updatePageInfo();
      $view->total_rows = $view->pager->getTotalItems();
    }
    catch (\Exception $e) {
      $view->result = [];
      if (!empty($view->live_preview)) {
        $this->messenger->addError($e->getMessage());
      }
      $this->logger->error($e->getMessage());
    }

    // Avoid notices about $view->execute_time being undefined if the query
    // doesn't finish.
    $view->execute_time = microtime(TRUE) - $start;
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions(): array {
    $options = parent::defineOptions();
    $options['csv_file'] = ['default' => ''];
    $options['header_index'] = ['default' => 0];
    $options['headers'] = ['default' => ''];
    $options['request_method'] = ['default' => 'get'];
    $options['request_body'] = ['default' => ''];
    $options['show_errors'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $form['csv_file'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('CSV File'),
      '#default_value' => static::getUriAsDisplayableString($this->options['csv_file']),
      '#description' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('Start typing the name of a CSV file to select it. You can also enter an internal path such as %path-to-file or an external URL such as %url.', [
            '%path-to-file' => '/files/data/example.csv',
            '%url' => 'https://example.com/files/data/example.csv',
          ]),
          $this->t('Note: Drupal token can be used as well.'),
          $this->t('If you would like to disable caching for a large, remote CSV, adjust the <a href="@link" target="_blank">cache duration settings</a>.', [
            '@link' => Url::fromRoute('views_csv_source.settings')->toString(),
          ]),
        ],
      ],
      '#maxlength' => 2048,
      '#target_type' => 'file',
      '#selection_handler' => 'default:views_csv_source_file_entity_selection',
      // Disable autocompletion when the first character is '/'.
      '#attributes' => ['data-autocomplete-first-character-blacklist' => '/'],
      // We are doing our own processing in static::getUriAsDisplayableString().
      '#process_default_value' => FALSE,
      '#element_validate' => [[static::class, 'validateCsvFileUriElement']],
      '#required' => TRUE,
    ];
    $form['header_index'] = [
      '#title' => $this->t('Header offset'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
      '#required' => FALSE,
      '#default_value' => $this->options['header_index'],
      '#description' => $this->t('The number of rows to skip before the header row.'),
    ];
    $form['headers'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Headers'),
      '#default_value' => $this->options['headers'],
      '#description' => $this->t("Headers to be passed for the REST call.<br />Pass the headers as CSV string. Ex:<br /><pre>{&quot;Authorization&quot;:&quot;Basic xxxxx&quot;,&quot;Content-Type&quot;:&quot;application/csv&quot;}</pre><br />.Here we are passing 2 headers for making the REST API call."),
      '#required' => FALSE,
    ];
    $form['request_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Request method'),
      '#default_value' => $this->options['request_method'],
      '#options' => [
        'get' => $this->t('GET'),
        'post' => $this->t('POST'),
      ],
      '#description' => $this->t('The request method to the REST call.'),
    ];
    $form['request_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Request body'),
      '#default_value' => $this->options['request_body'],
      '#states' => [
        'visible' => [
          'select[name="query[options][request_method]"]' => [
            'value' => 'post',
          ],
        ],
      ],
      '#description' => $this->t('The POST request body to the REST call.<br/>Pass the form values as CSV string. Ex: <br/><pre>[{&quot;name&quot;:&quot;item_key&quot;,&quot;contents&quot;:&quot;item value&quot;,&quot;headers&quot;:{&quot;Content-type&quot;:&quot;application/csv&quot;}}]</pre> See <a href="https://docs.guzzlephp.org/en/stable/request-options.html#multipart" target="_blank">the documentation for GuzzleHttp multipart request options</a>.'),
    ];
    $form['show_errors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show CSV errors'),
      '#default_value' => !empty($this->options['show_errors']),
      '#description' => $this->t('If there were any errors during CSV parsing, display them. It is recommended to leave this on during development.'),
      '#required' => FALSE,
    ];
  }

  /**
   * Add a field.
   *
   * @param string $table
   *   The table to add the field to.
   * @param mixed $field
   *   The field to add.
   * @param string $alias
   *   The alias to use for the field.
   * @param array $params
   *   Additional parameters for the field.
   *
   * @return mixed
   *   The field.
   */
  public function addField(string $table, mixed $field, string $alias = '', array $params = []): mixed {
    $alias = empty($alias) ? $field : $alias;

    // Add a field info array.
    if (empty($this->fields[$alias])) {
      $this->fields[$alias] = [
        'field' => $field,
        'table' => $table,
        'alias' => $alias,
      ] + $params;
    }

    return $alias;
  }

  /**
   * Add Order By.
   *
   * @param string|null $table
   *   The table to add the order by to.
   * @param mixed $field
   *   The field to order by.
   * @param string $orderby
   *   The order to use.
   */
  public function addOrderBy(string|null $table, mixed $field = NULL, string $orderby = 'ASC'): void {
    $this->orderby[] = ['field' => $field, 'order' => $orderby];
  }

  /**
   * Add Filter.
   *
   * @param mixed $filter
   *   The filter to add.
   */
  public function addFilter($filter): void {
    $this->filter[] = $filter;
  }

  /**
   * Adds a simple WHERE clause to the query.
   *
   * @param mixed $group
   *   The WHERE group to add these to; groups are used to create AND/OR
   *   sections. Groups cannot be nested. Use 0 as the default group.
   *   If the group does not yet exist, it will be created as an AND group.
   * @param string $column_name
   *   The name of the column name to check.
   * @param mixed $value
   *   The value to test the field against. In most cases, this is a scalar.
   *   For more complex options, it is an array. The meaning of each element in
   *   the array is dependent on the $operator.
   * @param string $operator
   *   The comparison operator, such as =, <, or >=.
   */
  public function addWhere(mixed $group, string $column_name, mixed $value = NULL, string $operator = '='): void {
    // Ensure all variants of 0 are actually 0.
    // Thus, '', 0 and NULL are all the default group.
    if (empty($group)) {
      $group = 0;
    }

    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }

    $this->where[$group]['conditions'][] = [
      'field' => $column_name,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * Retrieves the conditions placed on this query.
   *
   * @return array
   *   The conditions.
   */
  public function &getWhere(): array {
    return $this->where;
  }

  /**
   * To store the filter values required to pick the node from the csv.
   *
   * @param array $filter
   *   The filter values.
   */
  public function addContextualFilter(array $filter): void {
    $this->contextualFilter[] = $filter;
  }

  /**
   * To get the next filter value to pick the node from the csv.
   *
   * @return array
   *   The filter values.
   */
  public function getCurrentContextualFilter(): array {
    if (!isset($this->contextualFilter)) {
      return [];
    }

    $filter = current($this->contextualFilter);
    next($this->contextualFilter);
    return $filter;
  }

  /**
   * To store the filter values required to pick the node from the csv.
   *
   * @param array $filter
   *   The filter values.
   */
  public function addUrlParams(array $filter): void {
    $this->urlParams[] = $filter;
  }

  /**
   * To get the next filter value to pick the node from the csv.
   *
   * @return array
   *   The filter values.
   */
  public function getUrlParam(): array {
    if (!isset($this->urlParams)) {
      return [];
    }

    $filter = current($this->urlParams);
    next($this->urlParams);
    return $filter;
  }

  /**
   * Form element validation handler for the 'csv_file' element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form.
   */
  public static function validateCsvFileUriElement(array $element, FormStateInterface $form_state, array $form): void {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);

    // If getUserEnteredStringAsUri() mapped the entered value to an 'internal:'
    // URI, ensure the raw value begins with '/'.
    if (parse_url($uri, PHP_URL_SCHEME) === 'internal' && $element['#value'][0] !== '/') {
      $form_state->setError($element, new TranslatableMarkup('Manually entered paths should start with "/".'));
    }
  }

  /**
   * Get the aggregation options.
   *
   * @return array
   *   The aggregation options.
   */
  public function getAggregationInfo(): array {
    return [
      'group' => [
        'title' => $this->t('Group results together'),
        'is aggregate' => FALSE,
      ],
      'count' => [
        'title' => $this->t('Count'),
        'method' => 'aggregationMethodSimple',
      ],
      'sum' => [
        'title' => $this->t('Sum'),
        'method' => 'aggregationMethodSimple',
      ],
      'avg' => [
        'title' => $this->t('Average'),
        'method' => 'aggregationMethodSimple',
      ],
      'min' => [
        'title' => $this->t('Minimum'),
        'method' => 'aggregationMethodSimple',
      ],
      'max' => [
        'title' => $this->t('Maximum'),
        'method' => 'aggregationMethodSimple',
      ],
      // Add more aggregation types as needed.
    ];
  }

  /**
   * Aggregation method for simple aggregations.
   *
   * @param string $group_type
   *   The group type.
   * @param string $field
   *   The field.
   *
   * @return string
   *   The aggregation method.
   */
  public function aggregationMethodSimple(string $group_type, string $field): string {
    return strtoupper($group_type);
  }

  /**
   * Get the CSV headers.
   *
   * @return array
   *   The headers of the CSV file.
   */
  public function getCsvHeader(): array {
    return $this->connection->getCsvHeader($this->getCsvFileUri(), $this->getQueryOptions());
  }

  /**
   * Get the CSV column values for the given header.
   *
   * @param string $header
   *   The header to get the values for.
   *
   * @return array
   *   The values of the column.
   */
  public function getCsvColumnValues(string $header): array {
    return $this->connection->getCsvColumnData($this->getCsvFileUri(), $header, $this->getQueryOptions());
  }

  /**
   * Returns a list of non-aggregates to be added to the "group by" clause.
   *
   * Non-aggregates are fields that have no aggregation function (count, sum,
   * etc.) applied. Since the SQL standard requires all fields to either have
   * an aggregation function applied, or to be in the GROUP BY clause, Views
   * gathers those fields and adds them to the GROUP BY clause.
   *
   * @return array
   *   An array of the field names which are non-aggregates.
   */
  protected function getNonAggregates(): array {
    $non_aggregates = [];
    foreach ($this->fields as $alias => $field) {
      $string = '';
      $string .= $field['field'];
      $fieldname = $string;

      if (!empty($field['count'])) {
        // Retained for compatibility.
        $field['function'] = 'count';
      }
      if (!empty($field['function'])) {
        $this->hasAggregate = TRUE;
      }
      elseif (empty($field['aggregate'])) {
        $non_aggregates[$alias] = $fieldname;
      }
    }
    return $non_aggregates;
  }

  /**
   * Adds fields to the query.
   *
   * @param \Drupal\views_csv_source\Query\Select $query
   *   The drupal query object.
   */
  protected function compileFields(Select $query): void {
    foreach ($this->fields as $field) {
      if (!empty($field['count'])) {
        // Retained for compatibility.
        $field['function'] = 'count';
      }

      if (!empty($field['function'])) {
        $info = $this->getAggregationInfo();
        if (!empty($info[$field['function']]['method'])) {
          $query->addField($field['field'], $field['function'], $field['alias']);
        }
        $this->hasAggregate = TRUE;
      }
      elseif (empty($field['aggregate'])) {
        $query->addField($field['field'], '', $field['alias']);
      }

      if ($this->getCountOptimized) {
        // We only want the first field in this case.
        break;
      }
    }
  }

  /**
   * Construct the "WHERE" part of the query.
   *
   * As views has to wrap the conditions from arguments with AND, a special
   * group is wrapped around all conditions. This special group has the ID 0.
   * There is other code in filters that makes sure that the group IDs are
   * higher than zero.
   *
   * @param string $where
   *   Value is 'where'.
   *
   * @return \Drupal\Core\Database\Query\Condition
   *   The condition object.
   */
  protected function buildCondition(string $where = 'where'): Condition {
    $has_condition = FALSE;
    $has_arguments = FALSE;
    $has_filter = FALSE;

    $main_group = new Condition('AND');
    $filter_group = $this->groupOperator === 'OR' ? new Condition('OR') : new Condition('AND');

    foreach ($this->$where as $group => $info) {
      if (!empty($info['conditions'])) {
        $sub_group = $info['type'] == 'OR' ? new Condition('OR') : new Condition('AND');
        foreach ($info['conditions'] as $clause) {
          $has_condition = TRUE;
          $sub_group->condition($clause['field'], $clause['value'], $clause['operator']);
        }

        // Add the item to the filter group.
        if ($group != 0) {
          $has_filter = TRUE;
          $filter_group->condition($sub_group);
        }
        else {
          $has_arguments = TRUE;
          $main_group->condition($sub_group);
        }
      }
    }

    if ($has_filter) {
      $main_group->condition($filter_group);
    }

    if (!$has_arguments && $has_condition) {
      return $filter_group;
    }
    if ($has_arguments && $has_condition) {
      return $main_group;
    }
    return new Condition('AND');
  }

  /**
   * Get the CSV file URI.
   *
   * @return string
   *   The URI of the CSV file.
   */
  protected function getCsvFileUri(): string {
    // Replace any dynamic character if any.
    $uri = $this->options['csv_file'];

    // Replace any Drupal tokens in the url EG: [site:url].
    $uri = $this->token->replace($uri);

    while ($param = $this->getUrlParam()) {
      $uri = preg_replace('/' . preg_quote('%', '/') . '/', $param, $uri, 1);
    }
    return $uri;
  }

  /**
   * Get the query options.
   *
   * @return array
   *   The query options.
   */
  protected function getQueryOptions(): array {
    $query_options = $this->options + [
      'cache_id' => $this->view->id() . ':' . $this->view->current_display,
    ];
    if (!empty($query_options['csv_file'])) {
      $query_options['csv_file'] = $this->token->replace($query_options['csv_file']);
    }
    if (!$query_options['headers']) {
      $query_options['headers'] = ['Content-Type' => 'application/csv'];
    }
    else {
      $query_options['headers'] = Json::decode($query_options['headers']);
    }
    if (!empty($this->options['header_index'])) {
      $query_options['header_index'] = (int) $this->options['header_index'];
    }

    return $query_options;
  }

  /**
   * Builds row from result row.
   *
   * @param array $row
   *   The result row.
   * @param array $column_aliases
   *   Column aliases associated with their column names.
   *
   * @return \Drupal\views\ResultRow
   *   The result row.
   */
  protected function buildRowFromResultRow(array $row, array $column_aliases): ResultRow {
    $new_row = [];
    foreach ($column_aliases as $alias => $column) {
      $new_row[$alias] = !empty($row[$alias]) ? $row[$alias] : $row[$column];
      $new_row[$column] = $new_row[$alias];
    }

    return new ResultRow($new_row);
  }

  /**
   * Gets column aliases as array key values from fields.
   *
   * @return array
   *   The aliases as array keys associated with columns.
   */
  protected function getColumnAliasesAsKeyValues(): array {
    return array_map(
      fn ($field) => $field['field'],
      $this->fields
    );
  }

}
