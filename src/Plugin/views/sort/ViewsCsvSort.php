<?php

namespace Drupal\views_csv_source\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\views_csv_source\Plugin\views\ColumnSelectorTrait;

/**
 * Base sort handler for views_csv_source.
 *
 * @ViewsSort("views_csv_source_sort")
 */
class ViewsCsvSort extends SortPluginBase {

  use ColumnSelectorTrait;

  /**
   * {@inheritDoc}
   */
  public function defineOptions(): array {
    $options = parent::defineOptions();
    $options['key'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $form = $this->buildKeyOptionElement($form);
  }

  /**
   * {@inheritDoc}
   */
  public function query(): void {
    $this->query->addOrderBy(NULL, $this->options['key'], $this->options['order']);
  }

}
