<?php

namespace Drupal\views_csv_source\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views_csv_source\Plugin\views\ColumnSelectorTrait;

/**
 * Base field handler for views_csv_source.
 *
 * @ViewsField("views_csv_source_field")
 */
class ViewsCsvField extends FieldPluginBase {

  use ColumnSelectorTrait;

  /**
   * The table alias.
   *
   * @var string
   */
  public $tableAlias = '';

  /**
   * Render.
   */
  public function render(ResultRow $values) {
    if (!empty($this->options['trusted_html']) && ($value = $this->getValue($values))) {
      return [
        '#markup' => Markup::create($value),
      ];
    }

    return parent::render($values);
  }

  /**
   * Option definition.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['key'] = ['default' => ''];
    $options['trusted_html'] = ['default' => FALSE];
    return $options;
  }

  /**
   * Options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form = $this->buildKeyOptionElement($form);

    $form['trusted_html'] = [
      '#title' => $this->t('Trusted HTML'),
      '#description' => $this->t('This field is from a trusted source and contains raw HTML markup to render here. Use with caution.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['trusted_html']),
    ];
  }

  /**
   * Called to add the field to a query.
   */
  public function query() {
    // Add the field.
    $this->tableAlias = 'csv';

    $params = $this->options['group_type'] != 'group' ? ['function' => $this->options['group_type']] : [];

    $this->field_alias = $this->query->addField(
      $this->tableAlias,
      $this->options['key'],
      $this->getColumnAlias(),
      $this->options + $params,
    );
  }

  /**
   * Called to determine what to tell the click sorter.
   */
  public function clickSort($order) {
    if (isset($this->field_alias)) {
      $this->query->addOrderBy(NULL, $this->field_alias, $order);
    }
  }

}
