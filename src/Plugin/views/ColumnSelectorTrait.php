<?php

namespace Drupal\views_csv_source\Plugin\views;

use Drupal\Component\Utility\Html;
use Drupal\views_csv_source\Plugin\views\query\ViewsCsvQuery;

/**
 * Provides a trait for column selector.
 */
trait ColumnSelectorTrait {

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $summary = parent::adminSummary();
    if (empty($this->options['key'])) {
      return $summary;
    }
    return $summary ? $summary . " [Column: {$this->options['key']}]" : "[Column: {$this->options['key']}]";
  }

  /**
   * Build the key option element.
   *
   * @param array $form
   *   The form array.
   *
   * @return array
   *   The form array.
   */
  public function buildKeyOptionElement(array $form): array {
    $form['key'] = [
      '#title' => $this->t('Column Selector'),
      '#description' => $this->t('Choose a column'),
      '#default_value' => $this->options['key'],
      '#required' => TRUE,
    ];

    // Get the query.
    $query = $this->view->getQuery();
    // Ensure the query is of the correct type.
    if ($query instanceof ViewsCsvQuery && $headers = $query->getCsvHeader()) {
      $form['key']['#type'] = 'select';
      $form['key']['#options'] = array_combine($headers, $headers);
    }
    else {
      // Fall back to text value if CSV has not been selected.
      $form['key']['#type'] = 'textfield';
    }

    return $form;
  }

  /**
   * Provide an alias for the column.
   *
   * @return string
   *   The field alias.
   */
  public function getColumnAlias(): string {
    $key = Html::cleanCssIdentifier(strtolower($this->options['key']), [
      ' ' => '_',
      '_' => '_',
      '-' => '_',
    ]);
    return $key . '___' . $this->options['id'];
  }

}
