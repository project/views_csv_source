<?php

namespace Drupal\views_csv_source\Plugin\EntityReferenceSelection;

use Drupal\file\Plugin\EntityReferenceSelection\FileSelection;

/**
 * Provides a custom CSV file selection to only match files with csv mime type.
 *
 * @EntityReferenceSelection(
 *   id = "default:views_csv_source_file_entity_selection",
 *   label = @Translation("File selection"),
 *   entity_types = {"file"},
 *   group = "default",
 *   weight = 1
 * )
 */
class CsvFileSelection extends FileSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $query->condition('filemime', 'text/csv');
    return $query;
  }

}
