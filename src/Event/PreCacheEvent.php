<?php

namespace Drupal\views_csv_source\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event used to modify the CSV content before setting into cache.
 */
class PreCacheEvent extends Event {

  /**
   * Pre cache constant.
   */
  const VIEWS_CSV_SOURCE_PRE_CACHE = 'views_csv_source.pre_cache';

  /**
   * Constructs the object.
   *
   * @param string $cacheId
   *   The cache id.
   * @param string $data
   *   The views CSV query result body.
   */
  public function __construct(protected string $cacheId, protected string $data) {
  }

  /**
   * Get view executable.
   *
   * @return string
   *   Return cache id.
   */
  public function getCacheId(): string {
    return $this->cacheId;
  }

  /**
   * Get the data.
   *
   * @return string
   *   Return data.
   */
  public function getData(): string {
    return $this->data;
  }

  /**
   * Set the data for this event.
   *
   * @param string $data
   *   The data to override result data.
   */
  public function setData(string $data) {
    $this->data = $data;
  }

}
