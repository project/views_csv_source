/**
 * @file
 * Behaviors views csv source in the views admin screen.
 */

(function (Drupal, once) {
  Drupal.behaviors.viewsCsvSourceViewsAdmin = {
    attach(context) {
      once(
        'views-csv-source-select-filter-settings-ajax-initialized',
        '[data-views-csv-source-select-filter-settings]',
        context,
      ).forEach((settingsWrapper) => {
        const button = settingsWrapper.querySelector(
          '[data-drupal-selector="edit-options-views-csv-source-filter-select-settings-button"]',
        );
        // Hiding the button.
        button.style.display = 'none';

        // Track any changes inside the wrapper and click the refresh button to
        // trigger ajax.
        settingsWrapper.addEventListener('input', (e) => {
          button.click();
        });
      });
    },
  };
})(Drupal, once);
