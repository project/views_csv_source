<?php

/**
 * @file
 * Views csv source post-update file.
 */

/**
 * Ensure views with CSV as their base table has the proper query type.
 */
function views_csv_source_post_update_ensure_views_csv_have_proper_query_type(&$sandbox) {
  $view_storage = \Drupal::entityTypeManager()->getStorage('view');
  $view_ids = $view_storage->getQuery()
    ->accessCheck(FALSE)
    ->condition('base_table', 'csv')
    ->condition('display.*.display_options.query.type', 'views_query', '=')
    ->execute();

  if (!$view_ids) {
    return t("This site did not have any views base table CSV that didn't have the proper query type views_csv_source_query.");
  }

  $labels = [];
  foreach ($view_ids as $view_id) {
    /** @var \Drupal\views\ViewEntityInterface $view */
    $view = $view_storage->load($view_id);
    $displays = $view->get('display');
    foreach ($displays as $id => $display) {
      $displays[$id]['display_options']['query']['type'] = 'views_csv_source_query';
    }
    $view->set('display', $displays);
    $view->save();
    $labels[] = $view->label();
  }

  return t('The following views have been updated to set their query type to "views_csv_source_query": @views', [
    '@views' => implode(', ', $labels),
  ]);
}
