<?php

/**
 * @file
 * Views related hooks for views_csv_source.
 */

/**
 * Implements hook_views_data().
 */
function views_csv_source_views_data() {
  $data = [];
  $data['csv']['table']['group'] = t('CSV');

  $data['csv']['table']['base'] = [
    'title' => t('CSV'),
    'help' => t('Queries a CSV file.'),
    'query_id' => 'views_csv_source_query',
  ];

  $data['csv']['value'] = [
    'title' => t('CSV Field'),
    'help' => t('Name of the column in the CSV.'),
    'field' => [
      'id' => 'views_csv_source_field',
    ],
    'sort' => [
      'id' => 'views_csv_source_sort',
    ],
    'filter' => [
      'id' => 'views_csv_source_filter',
    ],
    'argument' => [
      'id' => 'views_csv_source_argument',
    ],
  ];

  $data['csv']['value_select'] = [
    'title' => t('CSV Field Options'),
    'help' => t('CSV filter that allows to select value based on column option values.'),
    'filter' => [
      'id' => 'views_csv_source_filter_select',
    ],
  ];

  return $data;
}
