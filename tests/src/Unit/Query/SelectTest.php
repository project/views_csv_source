<?php

namespace Drupal\Tests\views_csv_source\Unit\Query;

use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\views_csv_source\Traits\ResourcePathRetrieverTrait;
use Drupal\views_csv_source\Query\Connection;
use Drupal\views_csv_source\Query\Select;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\views_csv_source\Query\Select
 *
 * @group views_csv_source
 */
class SelectTest extends UnitTestCase {

  use ResourcePathRetrieverTrait;

  /**
   * The connection object.
   *
   * @var \Drupal\views_csv_source\Query\Connection
   */
  protected $connection;

  /**
   * The URI of the CSV file.
   *
   * @var string
   */
  protected string $csvUri = '';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->csvUri = $this->retrieveResource();

    $this->connection = new Connection(
      $this->createMock('Drupal\Core\Cache\CacheBackendInterface'),
      $this->createHttpClientMock(),
      $this->createMock('Drupal\Component\Datetime\TimeInterface'),
      $this->createMock('Symfony\Contracts\EventDispatcher\EventDispatcherInterface'),
      $this->getConfigFactoryStub(['views_csv_source.settings' => ['cache_ttl' => 3600]]),
      $this->createMock('Drupal\Core\Logger\LoggerChannelFactoryInterface'),
    );
  }

  /**
   * @covers ::__construct
   * @covers ::execute
   * @covers ::getRecords
   */
  public function testSimpleExecute() {
    $select = $this->getSelectQuery();
    $this->assertInstanceOf(Select::class, $select);
    $records = $select->execute();
    // Expecting an array since we didn't add any fields.
    $this->assertIsIterable($records);
    // Expecting zero here since we didn't add any fields.
    $this->assertCount(0, $records);
  }

  /**
   * @covers ::addField
   * @covers ::addColumn
   * @covers ::execute
   * @covers ::getRecords
   *
   * @dataProvider executeWithFieldsDataProvider
   */
  public function testExecuteWithFields(array $options_overrides, int $expected_count, $expected_result_index, array $range) {
    $select = $this->getSelectQuery($options_overrides)
      ->addField('Geography name')
      ->addField('Occupation Name');

    if (!empty($range['offset'])) {
      $select->range($range['offset'], $range['limit']);
    }
    $records = $select->execute();

    $records = array_values(iterator_to_array($records));

    $this->assertCount($expected_count, $records);

    $this->assertArrayHasKey('Geography name', $records[0]);
    $this->assertArrayHasKey('Occupation Name', $records[0]);
    $this->assertArrayNotHasKey('Total count', $records[0]);

    $this->assertEquals([
      'Geography name' => 'Hawaii',
      'Occupation Name' => 'Health Practitioner Support Technologists and Technicians',
    ], $records[$expected_result_index]);

    // Restore the proper csv uri just in case it's cached.
    $this->csvUri = $this->retrieveResource();
  }

  /**
   * @covers ::addField
   * @covers ::addColumn
   * @covers ::condition
   * @covers ::execute
   * @covers ::applyFilters
   *
   * @dataProvider conditionDataProvider
   */
  public function testExecuteWithFieldAndCondition(ConditionInterface $condition, string $field, array $expected_first_record, int $expected_count) {
    $select = $this->getSelectQuery()
      ->addField($field)
      ->condition($condition);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount($expected_count, $records);
    $this->assertEquals($expected_first_record, $records[0] ?? []);
  }

  /**
   * @covers ::condition
   * @covers ::execute
   * @covers ::applyFilters
   * @covers ::getRecords
   * @covers ::getSelectedFields
   * @covers ::buildConditions
   * @covers ::conditionPassed
   * @covers ::verifyCondition
   */
  public function testExecuteWithFieldAndNestedConditions() {
    $filter_group = new Condition('AND');

    $sub_group = new Condition('OR');
    $sub_group->condition('Geography name', 'Hawaii');
    $sub_group->condition('Occupation Name', 'Technicians', 'contains');

    $filter_group->condition($sub_group);

    $select = $this->getSelectQuery()
      ->addField('Geography name')
      ->condition($filter_group);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(3, $records);
    $this->assertEquals([
      [
        'Geography name' => 'Alaska',
        'Occupation Name' => 'Emergency Medical Technicians and Paramedics',
      ],
      [
        'Geography name' => 'Hawaii',
        'Occupation Name' => 'Health Practitioner Support Technologists and Technicians',
      ],
      [
        'Geography name' => 'Kansas',
        'Occupation Name' => 'Emergency Medical Technicians and Paramedics',
      ],
    ], $records);
  }

  /**
   * @covers ::groupBy
   * @covers ::execute
   * @covers ::applyGroupBy
   * @covers ::applyFilters
   * @covers ::getRecords
   * @covers ::extractGroupedColumnsValuesFromRecord
   * @covers ::processGroupByAndAggregation
   */
  public function testGroupBy() {
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by')
      ->addField('Total count', 'sum')
      ->groupBy('Geography name');

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(5, $records);

    // Ensure that the first record is Alabama and the Total count is 101.
    $this->assertEquals([
      'Geography name' => 'Alabama',
      'Total count' => '101',
    ], $records[0]);
  }

  /**
   * @covers ::orderBy
   * @covers ::execute
   * @covers ::applyOrderBy
   * @covers ::getRecords
   *
   * @dataProvider orderByDataProvider
   */
  public function testOrderBy(string $order, string $expected_first, string $expected_last) {
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by')
      ->orderBy('Geography name', $order);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(5, $records);

    $this->assertEquals(['Geography name' => $expected_first], $records[0]);
    $this->assertEquals(['Geography name' => $expected_last], $records[4]);
  }

  /**
   * @covers ::orderBy
   * @covers ::execute
   * @covers ::applyOrderBy
   * @covers ::getRecords
   */
  public function testOrderByColumnNotAddedAsField() {
    $select = $this->getSelectQuery()
      ->addField('Geography name')
      ->orderBy('Occupation Name', Select::DIRECTION_DESC);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertEquals([
      'Geography name' => 'Alabama',
      'Occupation Name' => 'Massage Therapists',
    ], $records[0]);
  }

  /**
   * @covers ::groupBy
   * @covers ::orderBy
   * @covers ::execute
   * @covers ::applyGroupBy
   * @covers ::applyFilters
   * @covers ::getRecords
   * @covers ::extractGroupedColumnsValuesFromRecord
   * @covers ::processGroupByAndAggregation
   * @covers ::applyOrderBy
   */
  public function testGroupByWithOrderBy() {
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by')
      ->addField('Total count', 'sum')
      ->groupBy('Geography name')
      ->orderBy('Total count', Select::DIRECTION_DESC);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(5, $records);

    // Ensure that the first record is Alaska and the Total count is 9045.
    $this->assertEquals([
      'Geography name' => 'Alaska',
      'Total count' => '9045',
    ], $records[0]);
  }

  /**
   * @covers ::range
   */
  public function testGroupByWithOrderByAndRange() {
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by')
      ->addField('Total count', 'sum')
      ->groupBy('Geography name')
      ->orderBy('Total count', Select::DIRECTION_DESC)
      ->range(0, 2);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(2, $records);
    $this->assertEquals([
      [
        'Geography name' => 'Alaska',
        'Total count' => '9045',
      ],
      [
        'Geography name' => 'Hawaii',
        'Total count' => '543',
      ],
    ], $records);
  }

  /**
   * @covers ::range
   * @covers ::setLimit
   * @covers ::setOffset
   * @covers ::getLimit
   * @covers ::getOffset
   */
  public function testGroupByWithOrderByAndLimitAndOffset() {
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by')
      ->addField('Total count', 'sum')
      ->groupBy('Geography name')
      ->orderBy('Total count', Select::DIRECTION_DESC)
      ->setLimit(2)
      ->setOffset(1);

    $this->assertEquals(2, $select->getLimit());
    $this->assertEquals(1, $select->getOffset());

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(2, $records);

    $this->assertEquals([
      [
        'Geography name' => 'Hawaii',
        'Total count' => '543',
      ],
      [
        'Geography name' => 'Illinois',
        'Total count' => '76',
      ],
    ], $records);
  }

  /**
   * @covers ::applyGroupBy
   * @covers ::applyFilters
   * @covers ::getRecords
   * @covers ::getSelectedFields
   */
  public function testGroupByWithFilters() {
    $subgroup_1 = new Condition('OR');
    $subgroup_1->condition('Geography name', 'Hawaii');
    $subgroup_1->condition('Occupation Name', 'Health Practitioner Support Technologists and Technicians');
    $subgroup_1->condition('Geography name', 'Alabama');
    $subgroup_1->condition('Occupation Name', 'Counselors');

    $filter_group = new Condition('AND');
    $filter_group->condition($subgroup_1);

    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by')
      ->addField('Total count', 'sum')
      ->groupBy('Geography name')
      ->condition($filter_group);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));

    $this->assertCount(2, $records);
    // Ensure that the first record is Alabama and the Total count is 101.
    $this->assertEquals([
      [
        'Geography name' => 'Alabama',
        'Total count' => '101',
      ],
      [
        'Geography name' => 'Hawaii',
        'Total count' => '543',
      ],
    ], $records);
  }

  /**
   * @covers ::addField
   * @covers ::groupBy
   * @covers ::getRecords
   * @covers ::applyOrderBy
   *
   * @throws \League\Csv\Exception
   */
  public function testGroupByAndAggregationOnSameColumnWithAlias() {
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'count', 'geography_name_count')
      ->addField('Geography name', 'group_by', 'geography_name_value')
      ->orderBy('Geography name', Select::DIRECTION_DESC);

    $records = $select->execute();
    $records = array_values(iterator_to_array($records));
    $this->assertCount(5, $records);

    // Confirm if ordering is applying as expected.
    $this->assertEquals([
      'geography_name_value' => 'Kansas',
      'geography_name_count' => '1',
    ], $records[0]);
  }

  /**
   * @covers ::getRecords
   * @covers ::getSelectedAggregatedFields
   *
   * @throws \League\Csv\Exception
   */
  public function testCsvWithEmptyColumns() {
    $this->csvUri = $this->retrieveResource('/views_csv_data_with_empty_columns_test.csv');

    // Testing regular field select.
    $select = $this->getSelectQuery()
      ->addField('Geography name')
      ->addField('Occupation Name');
    $records = $select->execute();
    $this->assertCount(7, array_values(iterator_to_array($records)));

    // Testing if group by also still working because the csv is rebuilt.
    $select = $this->getSelectQuery()
      ->addField('Geography name', 'group_by');
    $records = $select->execute();
    $this->assertCount(5, array_values(iterator_to_array($records)));

    // Restore the proper csv uri just in case it's cached.
    $this->csvUri = $this->retrieveResource();
  }

  /**
   * Data provider for ::testExecuteWithFieldAndCondition().
   */
  public static function conditionDataProvider(): array {
    $conditions = [];

    // Simple equal operator.
    $condition = new Condition('AND');
    $condition->condition('Geography name', 'Hawaii');
    $conditions['simple_equal_operator'] = [
      $condition,
      'Geography name',
      ['Geography name' => 'Hawaii'],
      1,
    ];

    // Contains operator test.
    $condition = new Condition('AND');
    $condition->condition('Occupation Name', 'Technicians', 'contains');
    $conditions['contains_operator'] = [
      $condition,
      'Occupation Name',
      ['Occupation Name' => 'Emergency Medical Technicians and Paramedics'],
      3,
    ];

    // Contains any operator.
    $condition = new Condition('AND');
    $condition->condition('Keywords', ['Technicians', 'Assistants', 'Therapists'], 'contains any');
    $conditions['contains_any_operator'] = [
      $condition,
      'Occupation Name',
      [
        'Occupation Name' => 'Massage Therapists',
        'Keywords' => 'Therapists',
      ],
      5,
    ];

    // Contains none operator.
    $condition = new Condition('AND');
    $condition->condition('Keywords', ['Technicians', 'Assistants', 'Therapists'], 'contains none');
    $conditions['contains_none_operator'] = [
      $condition,
      'Occupation Name',
      [
        'Occupation Name' => 'Counselors',
        'Keywords' => 'Counselors',
      ],
      2,
    ];

    // Contains all operator.
    $condition = new Condition('AND');
    $condition->condition('Keywords', ['Technicians', 'Technologists'], 'contains all');
    $conditions['contains_all_operator'] = [
      $condition,
      'Occupation Name',
      [
        'Occupation Name' => 'Health Practitioner Support Technologists and Technicians',
        'Keywords' => 'Technologists, Technicians',
      ],
      1,
    ];

    // "Contains all" operator with a missing keyword.
    $condition = new Condition('AND');
    $condition->condition('Keywords', ['Technicians', 'Hygienists', 'Technologists'], 'contains all');
    $conditions['contains_all_operator_with_missing_word'] = [
      $condition,
      'Occupation Name',
      [],
      0,
    ];
    return $conditions;
  }

  /**
   * Data provider for ::testOrderBy().
   */
  public static function orderByDataProvider(): array {
    $asc_data = [
      Select::DIRECTION_ASC,
      'Alabama',
      'Kansas',
    ];
    $desc_data = [
      Select::DIRECTION_DESC,
      'Kansas',
      'Alabama',
    ];
    return [
      'select_direction_asc' => $asc_data,
      'select_direction_desc' => $desc_data,
      'lowercase_asc' => [0 => 'asc'] + $asc_data,
      'lowercase_desc' => [0 => 'desc'] + $desc_data,
    ];
  }

  /**
   * Data provider for ::executeWithFields().
   */
  public function executeWithFieldsDataProvider(): array {
    $count = 7;
    $header_index_overrides = [
      'header_index' => 1,
      'csv_file' => $this->retrieveResource('/views_csv_data_with_header_index_moved_test.csv'),
    ];
    return [
      'no_overrides' => [[], $count, 4, []],
      'header_index_overrides' => [
        $header_index_overrides,
        $count,
        4,
        [],
      ],
      'header_index_overrides_with_range' => [
        $header_index_overrides,
        4,
        2,
        ['offset' => 2, 'limit' => 4],
      ],
    ];
  }

  /**
   * Gets the select query object.
   *
   * @return \Drupal\views_csv_source\Query\Select
   *   The select query object.
   */
  protected function getSelectQuery(array $options = []): Select {
    if (!empty($options['csv_file'])) {
      $this->csvUri = $options['csv_file'];
    }
    return $this->connection->select($this->csvUri, $options + [
      'headers' => ['Content-Type' => 'application/csv'],
      'cache_id' => 'views_csv_source_data_test',
      'csv_file' => $this->csvUri,
    ]);
  }

  /**
   * Returns a mock object for the httpClient.
   *
   * @return \GuzzleHttp\Client
   *   The mocked object,
   */
  protected function createHttpClientMock(): HttpClient {
    // Mocking the Guzzle response to return the test CSV file content.
    $body = file_get_contents($this->csvUri);
    $mock = new MockHandler([
      new Response(200, [], $body),
    ]);
    $handler = HandlerStack::create($mock);
    return new HttpClient(['handler' => $handler]);
  }

}
