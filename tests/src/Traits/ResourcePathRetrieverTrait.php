<?php

namespace Drupal\Tests\views_csv_source\Traits;

/**
 * Resource file path retriever methods trait.
 */
trait ResourcePathRetrieverTrait {

  /**
   * Retrieve the full path name of the given filename resource.
   *
   * @param string $filename_path
   *   The file name path under the "resources" folder.
   *
   * @return string
   *   The path to the resource file name to retrieve.
   */
  protected function retrieveResource(string $filename_path = '/views_csv_data_test.csv'): string {
    return $this->resourcesPath() . $filename_path;
  }

  /**
   * Gets the absolute path.
   *
   * @return string
   *   The absolute path.
   */
  protected function absolutePath(): string {
    return dirname(__DIR__, 3);
  }

  /**
   * Gets the resources path.
   *
   * @return string
   *   The resources path.
   */
  protected function resourcesPath(): string {
    return $this->absolutePath() . '/tests/resources';
  }

}
