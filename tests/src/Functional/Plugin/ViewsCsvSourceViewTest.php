<?php

namespace Drupal\Tests\views_csv_source\Functional\Plugin;

use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\Tests\views_csv_source\Traits\ResourcePathRetrieverTrait;
use Drupal\file\Entity\File;
use Drupal\views\Views;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests various views plugins introduced by the views_csv_source module.
 *
 * @group views_csv_source.
 */
class ViewsCsvSourceViewTest extends ViewTestBase {

  use ResourcePathRetrieverTrait;

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_views_csv_source'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'views_csv_source',
    'views_csv_source_test',
    'views',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_csv_source_test']): void {
    parent::setUp($import_test_views, $modules);

    // Create a new file entity referenced in the views.
    $file = File::create([
      'uid' => 0,
      'filename' => 'druplicon.txt',
      'uri' => 'public://views_csv_data_test.csv',
      'filemime' => 'text/csv',
    ]);
    $file->setPermanent();
    file_put_contents($file->getFileUri(), file_get_contents($this->retrieveResource()));
    $file->save();
  }

  /**
   * Test views csv source.
   */
  public function testViewsCsvSource() {
    // Testing regular view and header index.
    $file = File::load(1);
    foreach (['regular', 'header_index'] as $test_type) {
      if ($test_type === 'header_index') {
        file_put_contents($file->getFileUri(), file_get_contents($this->retrieveResource('/views_csv_data_with_header_index_moved_test.csv')));
        $view = Views::getView('test_views_csv_source');
        $display = &$view->storage->getDisplay('default');
        $display['display_options']['query']['options']['header_index'] = 1;
        $view->save();
      }

      $this->drupalGet('test-views-csv-source');
      // Check that the page can be loaded.
      $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

      $this->assertSession()->elementsCount('xpath', '//thead/tr/th[text()="Geography name"]', 1);
      $this->assertSession()->elementsCount('xpath', '//thead/tr/th[text()="Occupation name"]', 1);

      $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Alabama")]', 2);
      $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Alaska")]', 2);
      $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Hawaii")]', 1);
      $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Illinois")]', 1);
      $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Kansas")]', 1);
    }

    $view = Views::getView('test_views_csv_source');
    $display = &$view->storage->getDisplay('default');

    // Set header index back to 0.
    $display['display_options']['query']['options']['header_index'] = 0;
    file_put_contents($file->getFileUri(), file_get_contents($this->retrieveResource()));

    // Testing full pager.
    $display['display_options']['pager']['options']['items_per_page'] = 3;
    $display['display_options']['pager']['type'] = 'full';
    $display['display_options']['pager']['options']['tags']['next'] = 'Next >';
    $display['display_options']['pager']['options']['tags']['last'] = 'Last >>';
    $view->save();

    $this->drupalGet('test-views-csv-source');
    $labels = [
      'Next >',
      'Last >>',
    ];
    foreach ($labels as $label) {
      $this->assertSession()->pageTextContains($label);
    }

    // Test filters with "contains any" operator on the
    // "views_csv_source_filter_select".
    $view = Views::getView('test_views_csv_source');
    $display = &$view->storage->getDisplay('default');
    $display['display_options']['filters']['value_select'] = [
      'id' => 'value_select',
      'table' => 'csv',
      'field' => 'value_select',
      'relationship' => 'none',
      'plugin_id' => 'views_csv_source_filter_select',
      'operator' => 'contains any',
      'key' => 'Keywords',
      'empty_cell_behavior' => 'remove',
      'multi_value_cell_separator' => ',',
      'value' => ['Assistants', 'Therapists'],
    ];
    $view->save();

    $this->drupalGet('test-views-csv-source');
    $this->assertSession()->elementsCount('xpath', '//tbody/tr', 2);
    $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Alabama")]', 1);
    $this->assertSession()->elementsCount('xpath', '//tbody/tr/td[starts-with(text(), "Illinois")]', 1);
  }

}
